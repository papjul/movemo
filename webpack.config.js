var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */
    .addEntry('client', './assets/js/client.js')
    .addEntry('app-old', './assets/js/app-old.js')
//    .addEntry('progression', './assets/js/progression.js')
//    .addEntry('relaxation', './assets/js/relaxation.js')
//    .addEntry('routine-run', './assets/js/routine-run.js')
    .addEntry('routine-view', './assets/js/routine-view.js')
    /*.copyFiles({
        from: './assets/js/sw/'
    })*/

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
//    .disableSingleRuntimeChunk()
    .enableSingleRuntimeChunk()

    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    .enableEslintLoader()

    // uncomment if you use Sass/SCSS files
    .enableSassLoader()

    // uncomment if you're having problems with a jQuery plugin
    //.autoProvidejQuery()

    // uncomment if you use API Platform Admin (composer req api-admin)
    .enableReactPreset()
    //.addEntry('admin', './assets/js/admin.js')

    .configureBabel(function(babelConfig) {
        babelConfig.plugins.push("@babel/plugin-proposal-class-properties");
    });

module.exports = Encore.getWebpackConfig();
