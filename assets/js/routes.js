//import Skills from "./pages/skill/Skills"; // FIXME: window is not defined
import Account from "./pages/account/Account";
import Exercise from "./pages/exercise/Exercise";
import Exercises from "./pages/exercise/Exercises";
import Homepage from "./pages/Homepage";
import Relaxation from "./pages/relaxation/Relaxation";
import Routine from "./pages/routine/Routine";
import RoutineRun from "./pages/routine/RoutineRun";
import Routines from "./pages/routine/Routines";

const privateRoutes = [
    {
        path: "/:locale(en|fr)/routines/:name_id/run",
        component: RoutineRun,
    },
    {
        path: "/:locale(en|fr)/routines/:name_id",
        component: Routine,
    },
    {
        path: "/:locale(en|fr)/routines",
        component: Routines,
    },
    {
        path: "/:locale(en|fr)/exercises/:name_id",
        component: Exercise,
    },
    {
        path: "/:locale(en|fr)/exercises",
        component: Exercises,
    },
    /*{
        path: "/:locale(en|fr)/skills",
        component: Skills,
    },*/
    {
        path: "/:locale(en|fr)/account",
        component: Account,
    },
    {
        path: "/:locale(en|fr)?",
        component: Homepage,
    },
    // etc.
];

const publicRoutes = [
    {
        path: "/:locale(en|fr)/relaxation",
        component: Relaxation,
    },
];

export {
    privateRoutes,
    publicRoutes,
};