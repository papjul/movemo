import ReactOnRails from "react-on-rails";

import MovemoApp from "./app";
import MovemoStore from "./redux/store";

require("intl/locale-data/jsonp/en");
require("intl/locale-data/jsonp/fr");

ReactOnRails.register({MovemoApp});
ReactOnRails.registerStore({MovemoStore});