import IconButton from "@material-ui/core/IconButton";
import InputBase from "@material-ui/core/InputBase";
import Paper from "@material-ui/core/Paper";
import {makeStyles} from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import _ from "lodash";
import PropTypes from "prop-types";
import React, {useEffect, useState} from "react";
import {useIntl} from "react-intl";
import Loading from "../../components/Loading";
import RoutineCategoryList from "../../components/routine/RoutineCategoryList";
import {TABS} from "../../constants";
import Layout from "../../containers/layout/Layout";

const useStyles = makeStyles((theme) => ({
    paper: {
        position: "sticky",
        top: 80,
        padding: "2px 4px",
        display: "flex",
        alignItems: "center",
        marginBottom: theme.spacing(2),
        width: "100%"
    },
    input: {
        marginLeft: 8,
        flex: 1,
    },
    iconButton: {
        padding: 10,
    }
}));

const Routines = ({actions, config, entities}) => {
    const intl = useIntl();
    const classes = useStyles();
    const [filterText, setFilterText] = useState("");

    useEffect(() => {
        if (_.isEmpty(entities.routineCategories)) {
            const {fetchRoutineCategories} = actions;
            fetchRoutineCategories(config.locale, config.authToken);
        }
    }, []);

    if (config.isFetching || _.isEmpty(entities.routineCategories)) {
        return (
            <Layout{...{actions, config}} pageTitle={intl.formatMessage({id: "title.routines"})}
                   pageTab={TABS.ROUTINES}>
                <Loading />
            </Layout>
        );
    } else {
        return (
            <Layout{...{actions, config}} pageTitle={intl.formatMessage({id: "title.routines"})}
                   pageTab={TABS.ROUTINES}
                   searchChangeEvent={setFilterText} searchValue={filterText}
                   searchLabel={intl.formatMessage({id: "routine.search_for"})}>
                    {/* TODO: Move to component */}
                <Paper className={classes.paper} sx={{ display: { lg: 'block', xs: 'none' } }}>
                    <IconButton aria-label="search" className={classes.iconButton}>
                        <SearchIcon />
                    </IconButton>
                    <InputBase
                        autoFocus
                        onChange={(event) => {
                            setFilterText(event.target.value);
                        }}
                        className={classes.input}
                        placeholder={intl.formatMessage({id: "routine.search_for"})}
                        inputProps={{"aria-label": intl.formatMessage({id: "routine.search_for"})}}
                    />
                </Paper>
                <RoutineCategoryList routineCategories={entities.routineCategories} routines={entities.routines}
                                     locale={config.locale} filterText={filterText} />
            </Layout>
        );
    }
};

Routines.propTypes = {
    actions: PropTypes.shape({
        fetchRoutineCategories: PropTypes.func,
        setFont: PropTypes.func,
        setLocale: PropTypes.func,
        setTheme: PropTypes.func
    }).isRequired,
    config: PropTypes.shape({
        isFetching: PropTypes.bool,
        font: PropTypes.string,
        locale: PropTypes.string,
        theme: PropTypes.string,
        //$$routines: PropTypes.arrayOf(RoutinesPropTypes),
    }).isRequired
};

export default Routines;
