import PropTypes from "prop-types";
import React, {useEffect} from "react";
import {useIntl} from "react-intl";
import {useParams} from "react-router-dom";
import Loading from "../../components/Loading";
import RoutineView from "../../components/routine/RoutineView";
import {TABS} from "../../constants";
import Layout from "../../containers/layout/Layout";

const Routine = ({actions, config, entities}) => {
    const params = useParams();
    const intl = useIntl();

    useEffect(() => {
        if (!entities.routines || !(params.name_id in entities.routines)) {
            const {fetchRoutine} = actions;
            fetchRoutine(config.locale, config.authToken, params.name_id);
        }
    }, []);

    if (config.isFetching || !entities.routines || !(params.name_id in entities.routines)) {
        return (
            <Layout{...{actions, config}} pageTab={TABS.ROUTINES}>
                <Loading />
            </Layout>
        );
    } else {
        return (
            <Layout{...{actions, config}} pageTab={TABS.ROUTINES}
                   pageTitle={intl.formatMessage({id: "title.routine"}, {name: entities.routines[params.name_id].translations[config.locale].name})}>
                <RoutineView routine={entities.routines[params.name_id]} locale={config.locale} />
            </Layout>
        );
    }
};

Routine.propTypes = {
    actions: PropTypes.shape({
        fetchRoutine: PropTypes.func,
        setFont: PropTypes.func,
        setLocale: PropTypes.func,
        setTheme: PropTypes.func
    }).isRequired,
    config: PropTypes.shape({
        isFetching: PropTypes.bool,
        font: PropTypes.string,
        locale: PropTypes.string,
        theme: PropTypes.string,
        //$$routine: PropTypes.arrayOf(RoutinePropTypes),
    }).isRequired
};

export default Routine;
