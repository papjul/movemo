import _ from "lodash";
import PropTypes from "prop-types";
import React, {useEffect} from "react";
import {useIntl} from "react-intl";
import {useParams} from "react-router-dom";
import Loading from "../../components/Loading";
import RoutineTimer from "../../components/routine/RoutineTimer";
import Layout from "../../containers/layout/Layout";

const RoutineRun = ({actions, config, entities}) => {
    const params = useParams();
    const intl = useIntl();

    useEffect(() => {
        if (!entities.routines || !(params.name_id in entities.routines) || !('items' in entities.routines[params.name_id])) {
            const {fetchRoutine} = actions;
            fetchRoutine(config.locale, config.authToken, params.name_id);
        }
    }, []);

    if (config.isFetching || !entities.routines || !(params.name_id in entities.routines)) {
        return (
            <Layout{...{actions, config}} disableBottom disableGutters>
                <Loading />
            </Layout>
        );
    } else if (!('items' in entities.routines[params.name_id])) {
        return (
            <Layout{...{actions, config}} disableBottom disableGutters
                   pageTitle={intl.formatMessage({id: "title.routine"}, {name: entities.routines[params.name_id].translations[config.locale].name})}>
                <Loading />
            </Layout>
        );
    } else if (_.isEmpty(entities.routines[params.name_id].items)) {
        return (
            <Layout{...{actions, config}}>
                <p>Cette routine ne contient aucun exercise.</p>{/* TODO: Translate */}
            </Layout>
        );
    } else {
        return (
            <Layout{...{actions, config}} disableBottom disableGutters
                   pageTitle={intl.formatMessage({id: "title.routine"}, {name: entities.routines[params.name_id].translations[config.locale].name})}>
                <RoutineTimer locale={config.locale} routineItems={entities.routines[params.name_id].items} />
            </Layout>
        );
    }
};

RoutineRun.propTypes = {
    actions: PropTypes.shape({
        fetchRoutine: PropTypes.func,
        setFont: PropTypes.func,
        setLocale: PropTypes.func,
        setTheme: PropTypes.func
    }).isRequired,
    config: PropTypes.shape({
        isFetching: PropTypes.bool,
        font: PropTypes.string,
        locale: PropTypes.string,
        theme: PropTypes.string,
        //$$routineItems: PropTypes.arrayOf(RoutineItemsPropTypes),
    }).isRequired
};

export default RoutineRun;