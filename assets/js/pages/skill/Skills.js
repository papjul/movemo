import {Link} from "@material-ui/core";
import Container from "@material-ui/core/Container";
import _ from "lodash";
import PropTypes from "prop-types";
import React, {useEffect} from "react";
import {useIntl} from "react-intl";
import Loading from "../../components/Loading";
import SkillGraph from "../../components/skill/SkillGraph";
import Layout from "../../containers/layout/Layout";

// TODO: Translate
const Skills = ({actions, config, entities}) => {
    const intl = useIntl();

    useEffect(() => {
        if (_.isEmpty(entities.skills)) {
            const {fetchSkills} = actions;
            fetchSkills(config.locale, config.authToken);
        }
        if (_.isEmpty(entities.progressionExercises)) {
            const {fetchProgressionExercises} = actions;
            fetchProgressionExercises(config.locale, config.authToken);
        }
    }, []);

    if (_.isEmpty(entities.skills) || _.isEmpty(entities.progressionExercises)) {
        return (
            <Layout{...{actions, config}}>
                <Loading />
            </Layout>
        );
    } else {
        return (
            <Layout{...{actions, config}} fluid disableGutters>
                <SkillGraph progressions={entities.skills} exercises={entities.progressionExercises}
                            locale={config.locale} />
                <Container>
                    <p><strong>{intl.formatMessage({id: 'skills.original_source'})}</strong>{config.locale === "fr" && String.fromCharCode(8239)}: <Link
                        href="https://docs.google.com/spreadsheets/d/1a8tlZ-zbF695HA3Lmm20OIYeYYxo1lmUOczUXKLoL4s/"
                        rel="nofollow noreferrer">Bodyweight Fitness Progressions</Link>, by shellerik</p>

                    <p>{intl.formatMessage({id: 'skills.references'})}{config.locale === "fr" && String.fromCharCode(8239)}:</p>
                    <ul>
                        <li><Link
                            href="https://docs.google.com/spreadsheets/d/19l4tVfdTJLheLMwZBYqcw1oeEBPRh8mxngqrCz2YnVg/"
                            rel="nofollow noreferrer">Overcoming Gravity 2nd Edition — Exercise Chart</Link>
                        </li>
                        <li><Link href="https://www.reddit.com/r/bodyweightfitness/wiki/kb/recommended_routine"
                                  rel="nofollow noreferrer">Reddit Bodyweight Fitness — Recommended Routine</Link>
                        </li>
                        <li><Link href="https://www.reddit.com/r/bodyweightfitness/wiki/playground"
                                  rel="nofollow noreferrer">Reddit Bodyweight Fitness — Exercise Wiki</Link></li>
                        <li><Link href="https://antranik.org/bodyweight-training/" rel="nofollow noreferrer">Antranik
                            — Bodyweight Training</Link></li>
                    </ul>
                </Container>
            </Layout>
        );
    }
};

Skills.propTypes = {
    actions: PropTypes.shape({
        fetchSkills: PropTypes.func,
        fetchProgressionExercises: PropTypes.func,
        setFont: PropTypes.func,
        setLocale: PropTypes.func,
        setTheme: PropTypes.func
    }).isRequired,
    config: PropTypes.shape({
        isFetching: PropTypes.bool,
        font: PropTypes.string,
        locale: PropTypes.string,
        theme: PropTypes.string,
        //$$skills: PropTypes.arrayOf(SkillsPropTypes),
        //$$progressionExercises: PropTypes.arrayOf(ProgressionExercisesPropTypes),
    }).isRequired
};

export default Skills;
