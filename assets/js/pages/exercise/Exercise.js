import PropTypes from "prop-types";
import React, {useEffect} from "react";
import {useIntl} from "react-intl";
import {useParams} from "react-router-dom";
import ExerciseView from "../../components/exercise/ExerciseView";
import Loading from "../../components/Loading";
import {TABS} from "../../constants";
import Layout from "../../containers/layout/Layout";

const Exercise = ({actions, config, entities}) => {
    const params = useParams();
    const intl = useIntl();

    useEffect(() => {
        if (!entities.exercises || !(params.name_id in entities.exercises)) {
            const {fetchExercise} = actions;
            fetchExercise(config.locale, config.authToken, params.name_id);
        }
    }, []);

    if (config.isFetching || !entities.exercises || !(params.name_id in entities.exercises)) {
        return (
            <Layout{...{actions, config}} pageTab={TABS.EXERCISES}>
                <Loading />
            </Layout>
        );
    } else {
        return (
            <Layout{...{actions, config}}
                   pageTitle={intl.formatMessage({id: "title.exercise"}, {name: entities.exercises[params.name_id].translations[config.locale].name})}
                   pageTab={TABS.EXERCISES}>
                <ExerciseView exercise={entities.exercises[params.name_id]} locale={config.locale} />
            </Layout>
        );
    }
};

Exercise.propTypes = {
    actions: PropTypes.shape({
        fetchExercise: PropTypes.func,
        setFont: PropTypes.func,
        setLocale: PropTypes.func,
        setTheme: PropTypes.func,
    }).isRequired,
    config: PropTypes.shape({
        isFetching: PropTypes.bool,
        font: PropTypes.string,
        locale: PropTypes.string,
        theme: PropTypes.string,
        //$$exercise: PropTypes.arrayOf(ExercisePropTypes),
    }).isRequired
};

export default Exercise;
