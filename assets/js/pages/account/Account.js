import PropTypes from "prop-types";
import React from "react";
import {useIntl} from "react-intl";
import ChangePassword from "../../components/account/ChangePassword";
import {TABS} from "../../constants";
import Layout from "../../containers/layout/Layout";

const Account = ({actions, config}) => {
    const intl = useIntl();

    /*useEffect(() => {
        if (!data.get('$$changePasswordForm')) {
            const {fetchChangePasswordForm} = actions;
            fetchChangePasswordForm(data.get('locale'));
        }
    }, []);*/

    /*if (data.get('isFetching') || !data.get('$$exercises')) {
        return (
            <Layout{...{actions, data}} pageTitle={intl.formatMessage({id: 'title.account'})} pageTab={TABS.ACCOUNT}>
                <Loading/>
            </Layout>
        );
    } else {*/
    return (
        <Layout{...{actions, config}} pageTitle={intl.formatMessage({id: "title.account"})} pageTab={TABS.ACCOUNT}>
            <ChangePassword{...{actions, config}} />
        </Layout>
    );
    //}
};

Account.propTypes = {
    actions: PropTypes.shape({
        //fetchChangePasswordForm: PropTypes.func,
        setFont: PropTypes.func,
        setLocale: PropTypes.func,
        setTheme: PropTypes.func,
    }).isRequired,
    config: PropTypes.shape({
        isFetching: PropTypes.bool,
        font: PropTypes.string,
        locale: PropTypes.string,
        theme: PropTypes.string,
        //$$changePasswordForm: PropTypes.arrayOf(ChangePasswordSchemaPropTypes),
    }).isRequired
};

export default Account;