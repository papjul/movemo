import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import InputLabel from "@material-ui/core/InputLabel";
import MuiLink from "@material-ui/core/Link";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import Paper from "@material-ui/core/Paper";
import {makeStyles} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import {Visibility, VisibilityOff} from "@material-ui/icons";
import LoadingButton from "@material-ui/lab/LoadingButton";
import PropTypes from "prop-types";
import React, {useState} from "react";
import {useIntl} from "react-intl";
import {Link} from "react-router-dom";
import {TABS} from "../../constants";
import Layout from "../../containers/layout/Layout";
import {generateUrl} from "../../utils/routing";

const useStyles = makeStyles((theme) => ({
    paper: {
        minHeight: "calc(100vh - 56px - 56px - " + theme.spacing(6) + ")",
        height: "calc(100vh - 56px - 56px - " + theme.spacing(6) + ")",
        [`${theme.breakpoints.up("xs")} and (orientation: landscape)`]: {
            minHeight: "calc(100vh - 48px - 56px - " + theme.spacing(6) + ")",
            height: "calc(100vh - 48px - 56px - " + theme.spacing(6) + ")",
        },
        [theme.breakpoints.up("sm")]: {
            minHeight: "calc(100vh - 64px - 56px - " + theme.spacing(6) + ") !important",
            height: "calc(100vh - 64px - 56px - " + theme.spacing(6) + ") !important",
        },
        [theme.breakpoints.up("lg")]: {
            minHeight: "calc(100vh - 56px - 82px - " + theme.spacing(6) + ")",
            height: "calc(100vh - 56px - 82px - " + theme.spacing(6) + ")",
        }
    },
    form: {
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    },
    loading: {
        marginLeft: theme.spacing(2)
    },
    container: {
        width: "auto",
        marginLeft: "auto",
        marginRight: "auto",
        padding: `${theme.spacing(2)} ${theme.spacing(3)} ${theme.spacing(3)}`
    },
}));

const Login = ({actions, config}) => {
    const classes = useStyles();
    const intl = useIntl();
    //const history = useHistory()
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [showPassword, setShowPassword] = useState(false);

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const {login} = actions;
        login(config.locale, username, password);
    };

    return (
        <Layout{...{actions, config}} pageTitle={intl.formatMessage({id: "title.login"})} pageTab={TABS.ACCOUNT}>
            <Grid container className={classes.paper} alignItems="center">
                <Grid item className={classes.container} component={Paper} elevation={6}>
                    <Typography component="h1" variant="h5" align="center">
                        {intl.formatMessage({id: "title.login"})}
                    </Typography>
                    <form className={classes.form} onSubmit={handleSubmit} noValidate method="POST">
                        <TextField
                            value={username}
                            onInput={(e) => setUsername(e.target.value)}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="username"
                            label={intl.formatMessage({id: "login.username"})}
                            name="username"
                            autoComplete="username"
                            autoFocus
                        />
                        <FormControl variant="outlined" fullWidth>
                            <InputLabel htmlFor="password">{intl.formatMessage({id: "login.password"})}</InputLabel>
                            <OutlinedInput
                                value={password}
                                onInput={(e) => setPassword(e.target.value)}
                                required
                                name="password"
                                label={intl.formatMessage({id: "login.password"})}
                                type={showPassword ? "text" : "password"}
                                id="password"
                                autoComplete="current-password"
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={() => setShowPassword(!showPassword)}
                                            onMouseDown={handleMouseDownPassword}
                                        >
                                            {showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                        {config.isFetching
                            ? <LoadingButton
                                pending
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                aria-label={intl.formatMessage({id: "login.submit"})}
                            >
                                {intl.formatMessage({id: "login.submit"})}
                            </LoadingButton>
                            : <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                aria-label={intl.formatMessage({id: "login.submit"})}
                            >
                                {intl.formatMessage({id: "login.submit"})}
                            </Button>}
                    </form>

                    <Grid container justifyContent="space-between">
                        <Grid item>
                            <MuiLink
                                to={generateUrl("/password_reset", config.locale)}
                                component={Link}>{intl.formatMessage({id: "login.forgot_password"})}</MuiLink>
                        </Grid>
                        <Grid item>
                            <MuiLink
                                to={generateUrl("/register", config.locale)}
                                component={Link}>{intl.formatMessage({id: "login.register"})}</MuiLink>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Layout>
    );
};

Login.propTypes = {
    actions: PropTypes.shape({
        setFont: PropTypes.func,
        setLocale: PropTypes.func,
        setTheme: PropTypes.func,
    }).isRequired,
    config: PropTypes.shape({
        isFetching: PropTypes.bool,
        font: PropTypes.string,
        locale: PropTypes.string,
        theme: PropTypes.string,
    }).isRequired
};

export default Login;