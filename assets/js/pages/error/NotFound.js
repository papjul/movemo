import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Home from "@material-ui/icons/Home";
import PropTypes from "prop-types";
import React from "react";
import {useIntl} from "react-intl";
import {Link} from "react-router-dom";
import Layout from "../../containers/layout/Layout";
import {generateUrl} from "../../utils/routing";

const useStyles = makeStyles((theme) => ({
    icon: {
        width: 192,
        height: 192,
        color: theme.palette.secondary.main,
    },
    container: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        height: "100%",
    },
    paper: {
        backgroundColor: theme.palette.background.default,
        height: "100vh",
        margin: 0,
    },
    button: {
        marginTop: 20,
    },
}));

const PageNotFound = ({actions, config}) => {
    const intl = useIntl();
    const classes = useStyles();

    return (
        <Layout{...{actions, config}} pageTitle={intl.formatMessage({id: "title.404"})}>
            <Paper className={classes.paper}>
                <div className={classes.container}>
                    {/*<AppIcon className={classes.icon} />*/}
                    <Typography variant="h4">
                        {intl.formatMessage({id: "404.message"})}
                    </Typography>
                    <Typography variant="subtitle1">
                        {intl.formatMessage({id: "404.description"})}
                    </Typography>
                    <Button
                        variant="contained"
                        color="secondary"
                        aria-label={intl.formatMessage({id: "404.back_to_homepage"})}
                        href="/"
                        className={classes.button}
                        component={Link}
                        to={generateUrl("", config.locale)}
                    >
                        <Home /> {intl.formatMessage({id: "404.back_to_homepage"})}
                    </Button>
                </div>
            </Paper>
        </Layout>
    );
};

PageNotFound.propTypes = {
    actions: PropTypes.shape({
        setFont: PropTypes.func,
        setLocale: PropTypes.func,
        setTheme: PropTypes.func,
    }).isRequired,
    config: PropTypes.shape({
        isFetching: PropTypes.bool,
        font: PropTypes.string,
        locale: PropTypes.string,
        theme: PropTypes.string,
    }).isRequired
};

export default PageNotFound;