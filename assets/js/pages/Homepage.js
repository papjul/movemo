import _ from "lodash";
import PropTypes from "prop-types";
import React, {useEffect} from "react";
import {useIntl} from "react-intl";
import Loading from "../components/Loading";
import FeaturedRoutineList from "../components/routine/FeaturedRoutineList";
import {TABS} from "../constants";
import Layout from "../containers/layout/Layout";

const Homepage = ({actions, config, entities}) => {
    const intl = useIntl();

    useEffect(() => {
        if (_.isEmpty(entities.featuredRoutines)) {
            const {fetchFeaturedRoutines} = actions;
            fetchFeaturedRoutines(config.locale, config.authToken);
        }
    }, []);

    return (
        <Layout{...{actions, config}} pageTitle={intl.formatMessage({id: "title.home"})} pageTab={TABS.HOME}>
            {(config.isFetching || _.isEmpty(entities.featuredRoutines))
                ? <Loading />
                : <FeaturedRoutineList featuredRoutines={entities.featuredRoutines} locale={config.locale} />
            }
        </Layout>
    );
};

Homepage.propTypes = {
    actions: PropTypes.shape({
        fetchFeaturedRoutines: PropTypes.func,
        setFont: PropTypes.func,
        setLocale: PropTypes.func,
        setTheme: PropTypes.func,
    }).isRequired,
    config: PropTypes.shape({
        isFetching: PropTypes.bool,
        font: PropTypes.string,
        locale: PropTypes.string,
        theme: PropTypes.string,
        //$$featuredRoutines: PropTypes.arrayOf(FeaturedRoutinesPropTypes),
    }).isRequired
};

export default Homepage;