import PropTypes from "prop-types";
import React from "react";
import {useIntl} from "react-intl";
import Breathe from "../../components/relaxation/Breathe";
import {TABS} from "../../constants";
import Layout from "../../containers/layout/Layout";

const Relaxation = ({actions, config}) => {
    const intl = useIntl();

    return (
        <Layout{...{actions, config}} pageTitle={intl.formatMessage({id: "title.relaxation"})}
               pageTab={TABS.RELAXATION}>
            <Breathe />
        </Layout>
    );
};

Relaxation.propTypes = {
    actions: PropTypes.shape({
        setFont: PropTypes.func,
        setLocale: PropTypes.func,
        setTheme: PropTypes.func,
    }).isRequired,
    config: PropTypes.shape({
        isFetching: PropTypes.bool,
        font: PropTypes.string,
        locale: PropTypes.string,
        theme: PropTypes.string,
    }).isRequired
};

export default Relaxation;
