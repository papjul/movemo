import {defineMessages} from "react-intl";

const messages = defineMessages({
    "app_name": "Movemo",

    "title.home": "Movemo",
    "title.routines": "Routines",
    "title.routine": "{name}",
    "title.routine_run": "{name}",
    "title.exercises": "Exercices",
    "title.exercise": "{name}",
    "title.relaxation": "Relaxation",
    "title.skills": "Aptitudes (beta)",
    "title.account": "Compte",
    "title.change_password": "Modification du mot de passe",
    "title.login": "Connexion",
    "title.register": "Inscription",
    "title.404": "Page non trouvée",
    "title.settings": "Paramètres",

    "footer.license": "Licence AGPLv3 à venir",

    "settings.general": "Général",
    "settings.theme": "Thème",
    "settings.theme.light": "Clair",
    "settings.theme.dark": "Sombre",
    "settings.language": "Langue",
    "settings.accessibility": "Accessibilité",
    "settings.font": "Police",
    "settings.font.default": "Par défaut (Roboto)",
    "settings.font.luciole": "Déficience visuelle (Luciole)",
    "settings.font.opendys": "Dyslexie (OpenDyslexic)",

    "label.search_for": "Rechercher…",
    "label.apply": "Appliquer",
    "label.close": "Fermer",
    "label.save": "Enregistrer",
    "label.cancel": "Annuler",

    "home.featured_routines": "Routines mises en avant",
    "home.see_all": "Voir toutes les routines",

    "login.username": "Nom d’utilisateur",
    "login.password": "Mot de passe",
    "login.submit": "Connexion",
    "login.forgot_password": "Mot de passe oublié",
    "login.register": "Inscription",

    "404.message": "404 Page non trouvée",
    "404.description": "Nous sommes désolés mais la page que vous recherchez n’existe pas.",
    "404.back_to_homepage": "Retour à l’accueil",

    "breathe.inhale": "Inspiration (sec)",
    "breathe.hold_after_inhale": "Maintien après inspiration (sec)",
    "breathe.exhale": "Expiration (sec)",
    "breathe.hold_after_exhale": "Maintien après expiration (sec)",
    "breathe.duration": "Durée (min)",
    "breathe.refresh": "Rafraîchir",
    "breathe.preselection": "Préselection",
    "breathe.customize": "Personnaliser",

    "breathe.option.cardiac_coherence": "Cohérence cardiaque",
    "breathe.option.customized": "Personnalisé",

    "menu.home": "Routines",
    "menu.routines": "Routines",
    "menu.exercises": "Exercices",
    "menu.relaxation": "Relaxation",
    "menu.skills": "Aptitudes (beta)",
    "menu.change_language": "Changer la langue",
    "menu.change_theme": "Changer le thème",
    "menu.account": "Profil",
    "menu.open_settings": "Ouvrir les paramètres",

    "routine.search_for": "Rechercher une routine…",

    "routine.start": "Commencer",
    "routine.details": "Détails",

    "routine.author": "Auteur",
    "routine.license": "Licence",
    "routine.source": "Source",

    "routine.previous": "Précédent",
    "routine.next": "Suivant",
    "routine.next_exercise": "Prochain exercice",
    "routine.resume": "Reprendre",
    "routine.pause": "Pause",

    "exercise.search_for": "Rechercher un exercice…",
    "exercise.name": "Nom",
    "exercise.name_in_english": "Nom en anglais",
    "exercise.source": "Source",
    "exercise.description": "Description",
    "exercise.evolution": "Evolution",

    "skills.original_source": "Source originale",
    "skills.references": "Références",
});

export default messages;