import {defineMessages} from "react-intl";

const messages = defineMessages({
    "app_name": "Movemo",

    "title.home": "Movemo",
    "title.routines": "Routines",
    "title.routine": "{name}",
    "title.routine_run": "{name}",
    "title.exercises": "Exercises",
    "title.exercise": "{name}",
    "title.relaxation": "Relaxation",
    "title.skills": "Skills (beta)",
    "title.account": "Account",
    "title.change_password": "Change password",
    "title.login": "Log in",
    "title.register": "Register",
    "title.404": "Page not found",
    "title.settings": "Settings",

    "footer.license": "AGPLv3 license",

    "menu.home": "Routines",
    "menu.routines": "Routines",
    "menu.exercises": "Exercises",
    "menu.relaxation": "Relaxation",
    "menu.skills": "Skills (beta)",
    "menu.change_language": "Change language",
    "menu.change_theme": "Change theme",
    "menu.account": "Account",
    "menu.open_settings": "Open settings",

    "settings.general": "General",
    "settings.theme": "Theme",
    "settings.theme.light": "Light",
    "settings.theme.dark": "Dark",
    "settings.language": "Language",
    "settings.accessibility": "Accessibility",
    "settings.font": "Font",
    "settings.font.default": "Default (Roboto)",
    "settings.font.luciole": "Visual impairment (Luciole)",
    "settings.font.opendys": "Dyslexia (OpenDyslexic)",

    "label.search_for": "Search for…",
    "label.apply": "Apply",
    "label.close": "Close",
    "label.save": "Save",
    "label.cancel": "Cancel",

    "home.featured_routines": "Featured routines",
    "home.see_all": "See all routines",

    "login.username": "Username",
    "login.password": "Password",
    "login.submit": "Log in",
    "login.forgot_password": "Forgot password",
    "login.register": "Register",

    "404.message": "404 Page not found",
    "404.description": "We are sorry but the page you are looking for does not exist.",
    "404.back_to_homepage": "Back to homepage",

    "breathe.inhale": "Inhale (sec)",
    "breathe.hold_after_inhale": "Hold after inhale (sec)",
    "breathe.exhale": "Exhale (sec)",
    "breathe.hold_after_exhale": "Hold after exhale (sec)",
    "breathe.duration": "Duration (min)",
    "breathe.preselection": "Preselection",
    "breathe.customize": "Customize",

    "breathe.option.cardiac_coherence": "Cardiac coherence",
    "breathe.option.customized": "Customized",

    "routine.search_for": "Search for routine…",

    "routine.start": "Start routine",
    "routine.details": "Learn more",

    "routine.author": "Author",
    "routine.license": "License",
    "routine.source": "Source",

    "routine.previous": "Previous",
    "routine.next": "Next",
    "routine.next_exercise": "Next exercise",
    "routine.resume": "Resume",
    "routine.pause": "Pause",

    "exercise.search_for": "Search for exercise…",
    "exercise.name": "Name",
    "exercise.name_in_english": "Name in English",
    "exercise.source": "Source",
    "exercise.description": "Description",
    "exercise.evolution": "Evolution",

    "skills.original_source": "Original source",
    "skills.references": "References",
});

export default messages;