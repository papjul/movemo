import en_messages from "./en";
import fr_messages from "./fr";

const locales = [
    {
        locale: "en",
        messages: en_messages,
    },
    {
        locale: "fr",
        messages: fr_messages,
    },
];

export default locales;