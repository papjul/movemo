const LANGUAGES_LABEL = [
    {
        code: "en",
        text: "English",
    },
    {
        code: "fr",
        text: "Français",
    },
];

const LANGUAGES = ["en", "fr"];

const TABS = {
    "HOME": 0,
    "ROUTINES": 0,
    "EXERCISES": 1,
    "RELAXATION": 2,
    "SKILLS": null,
    "ACCOUNT": 3
};

module.exports = {
    LANGUAGES_LABEL,
    LANGUAGES,
    TABS
};