import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import {makeStyles} from "@material-ui/core/styles";
import Alert from "@material-ui/core/Alert";
import PropTypes from "prop-types";
import React from "react";
import {Helmet} from "react-helmet";
import {useIntl} from "react-intl";
import {useParams} from "react-router-dom";
import {LANGUAGES} from "../../constants";
import BottomDesktop from "./BottomDesktop";
import BottomMobile from "./BottomMobile";
import TopDesktop from "./TopDesktop";
import TopMobile from "./TopMobile";

const useStyles = makeStyles(theme => ({
    container: {
        paddingTop: theme.spacing(3),
        paddingBottom: `calc(56px + ${theme.spacing(3)})`, /* TODO: height of footer instead of "56" */
        [theme.breakpoints.down("md")]: {
            paddingBottom: `calc(56px + ${theme.spacing(3)})`
        }
    },
    javascriptAlert: {
        marginBottom: theme.spacing(3),
    }
}));

const Layout = ({actions, children, config, disableBottom, disableGutters, fluid, searchChangeEvent, searchLabel, searchValue, pageTab, pageTitle}) => {
    const params = useParams();
    const intl = useIntl();
    if (params.locale && LANGUAGES.indexOf(params.locale) !== -1 && params.locale !== config.locale) {
        actions.setLocale(params.locale);
    }
    const maxWidth = fluid ? false : "lg";
    const classes = useStyles();

    return (
        <>
            <Helmet>
                <title>{pageTitle ? pageTitle : intl.formatMessage({id: "app_name"})}</title>
            </Helmet>

            <Box sx={{ display: { lg: 'block', xs: 'none' } }}>
                <TopDesktop{...{actions, config}} />
            </Box>
            <Box sx={{ display: { lg: 'none', xs: 'block' } }}>
                <TopMobile{...{actions, config, searchChangeEvent, searchLabel, searchValue, pageTitle}} />
            </Box>

            <Container{...{disableGutters, maxWidth}} className={!disableGutters ? classes.container : null}>
                {/* TODO: Traduire */}
                <noscript>
                    <Alert severity="info" className={classes.javascriptAlert}>
                        Vous avez désactivé JavaScript et nous respectons ce choix.<br />
                        Toutefois, l’expérience sur le site sera très limitée. Pour vous convaincre de mettre Movemo sur
                        liste blanche, nous tenions à ce que vous sachiez que rien de malveillant n’est effectué en
                        JavaScript et aucun site tiers consulté, le code source pouvant être librement audité.<br />
                        Par ailleurs, sur l’aspect performances, la majorité du JavaScript est généré par le serveur, ne
                        laissant qu’une partie mineure à votre navigateur à prendre en compte.
                    </Alert>
                </noscript>
                {children}
            </Container>

            {!disableBottom && <>
                <Box sx={{ display: { lg: 'block', xs: 'none' } }}>
                    <BottomDesktop locale={config.locale} />
                </Box>
                <Box sx={{ display: { lg: 'none', xs: 'block' } }}>
                    <BottomMobile{...{pageTab}} locale={config.locale} />
                </Box>
            </>}
        </>
    );
};

Layout.propTypes = {
    actions: PropTypes.shape({
        setFont: PropTypes.func,
        setLocale: PropTypes.func,
        setTheme: PropTypes.func
    }).isRequired,
    config: PropTypes.shape({
        font: PropTypes.string,
        locale: PropTypes.string,
        theme: PropTypes.string
    }).isRequired
};

export default Layout;
