import {Paper} from "@material-ui/core";
import Container from "@material-ui/core/Container";
import {makeStyles} from "@material-ui/core/styles";
import PropTypes from "prop-types";
import React from "react";
import {useIntl} from "react-intl";

const useStyles = makeStyles(theme => ({
    bottomNavigation: {
        width: "100%",
        backgroundColor: theme.palette.background.paper,
        position: "fixed", // FIXME: Overlap
        bottom: 0,
    },
}));

const BottomDesktop = ({locale}) => {
    const intl = useIntl();
    const classes = useStyles();

    return (
        <Paper className={classes.bottomNavigation} component="footer">
            <Container>
                <p>Movemo &copy; 2020</p>
                <p>{intl.formatMessage({id: 'footer.license'})}</p>
            </Container>
        </Paper>
    );
};

BottomDesktop.propTypes = {
    locale: PropTypes.string.isRequired
};

export default BottomDesktop;