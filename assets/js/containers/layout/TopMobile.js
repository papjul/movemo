import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import InputBase from "@material-ui/core/InputBase";
import {makeStyles} from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import BackIcon from "@material-ui/icons/ArrowBack";
import ClearIcon from "@material-ui/icons/Clear";
import SearchIcon from "@material-ui/icons/Search";
import PropTypes from "prop-types";
import React, {useState} from "react";
import {useIntl} from "react-intl";
import {useHistory} from "react-router-dom";
import MenuSettings from "../../components/menu/MenuSettings";

const useStyles = makeStyles(theme => ({
    grow: {
        flex: "1 1 auto",
    },
    icon: {
        color: theme.palette.primary.contrastText,
        cursor: "pointer",
    },
    colorInherit: {
        color: "inherit"
    }
}));

const TopMobile = ({actions, config, searchChangeEvent, searchLabel, searchValue, pageTitle}) => {
    const history = useHistory(); // FIXME: Better handling?
    const intl = useIntl();
    const classes = useStyles();
    const [searchOpen, setSearchOpen] = useState(false);

    const openSearch = () => {
        setSearchOpen(true);
        //document.getElementById('app-search').focus(); // Doesn't work because state is not updated immediately, use autoFocus instead
    };
    const clearSearch = () => {
        searchChangeEvent("");
        document.getElementById("app-search").focus();
    };

    return (
        <AppBar position="sticky">
            <Toolbar>
                <IconButton
                    edge="start"
                    color="inherit"
                    aria-label="go back" // TODO
                    onClick={searchOpen ? () => setSearchOpen(false) : history.goBack}
                >
                    <BackIcon />
                </IconButton>

                {!searchOpen && <Typography variant="h6" color="inherit" noWrap>
                    {pageTitle ? pageTitle : intl.formatMessage({id: "app_name"})}
                </Typography>}

                {!searchOpen && <div className={classes.grow} />}

                {searchOpen &&
                <InputBase
                    id="app-search"
                    fullWidth
                    classes={{
                        root: classes.colorInherit
                    }}
                    placeholder={searchLabel ? searchLabel : intl.formatMessage({id: "label.search_for"})}
                    onChange={(e) => searchChangeEvent(e.target.value)}
                    value={searchValue}
                    autoFocus
                    endAdornment={
                        <InputAdornment position="end">
                            <IconButton className={classes.colorInherit} onClick={clearSearch}>
                                <ClearIcon />
                            </IconButton>
                        </InputAdornment>
                    }
                />}
                {searchChangeEvent !== undefined && !searchOpen &&
                <IconButton
                    color="inherit"
                    aria-label="search" // TODO
                    onClick={openSearch}
                >
                    <SearchIcon />
                </IconButton>
                }
                <MenuSettings {...{actions, config}} />
            </Toolbar>
        </AppBar>
    );
};

TopMobile.propTypes = {
    actions: PropTypes.shape({
        setFont: PropTypes.func,
        setLocale: PropTypes.func,
        setTheme: PropTypes.func
    }).isRequired,
    config: PropTypes.shape({
        font: PropTypes.string,
        locale: PropTypes.string,
        theme: PropTypes.string
    }).isRequired
    // TODO: Complete
};

export default TopMobile;