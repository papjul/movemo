import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import {makeStyles} from "@material-ui/core/styles";
import ExercisesIcon from "@material-ui/icons/FitnessCenter";
import HomeIcon from "@material-ui/icons/Home";
import AccountIcon from "@material-ui/icons/Person";
import RelaxationIcon from "@material-ui/icons/Spa";
import PropTypes from "prop-types";
import React from "react";
import {useIntl} from "react-intl";
import {Link} from "react-router-dom";
import {generateUrl} from "../../utils/routing";

const useStyles = makeStyles(theme => ({
    bottomNavigation: {
        width: "100%",
        position: "fixed", // FIXME: Overlap
        bottom: 0,
        border: 0,
        borderTop: "2px",
        borderStyle: "solid",
        borderColor: theme.palette.grey[500]
    },
}));

const BottomMobile = ({locale, pageTab}) => {
    const intl = useIntl();
    const classes = useStyles();

    return (
        <BottomNavigation className={classes.bottomNavigation} value={pageTab} showLabels>
            <BottomNavigationAction label={intl.formatMessage({id: "menu.home"})} icon={<HomeIcon />}
                                    component={Link}
                                    to={generateUrl("/", locale)} />
            <BottomNavigationAction label={intl.formatMessage({id: "menu.exercises"})} icon={<ExercisesIcon />}
                                    component={Link}
                                    to={generateUrl("/exercises", locale)} />
            <BottomNavigationAction label={intl.formatMessage({id: "menu.relaxation"})} icon={<RelaxationIcon />}
                                    component={Link}
                                    to={generateUrl("/relaxation", locale)} />
            <BottomNavigationAction label={intl.formatMessage({id: "menu.account"})} icon={<AccountIcon />}
                                    component={Link}
                                    to={generateUrl("/account", locale)} />
        </BottomNavigation>
    );
};

BottomMobile.propTypes = {
    locale: PropTypes.string.isRequired,
    pageTab: PropTypes.number
};

export default BottomMobile;