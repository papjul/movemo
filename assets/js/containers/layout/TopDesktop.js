import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import {makeStyles} from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import Brightness4Icon from "@material-ui/icons/Brightness4";
import BrightnessHighIcon from "@material-ui/icons/BrightnessHigh";
import ProfileIcon from "@material-ui/icons/Person";
import PropTypes from "prop-types";
import React from "react";
import {useIntl} from "react-intl";
import {Link} from "react-router-dom";
import MenuSelectLanguage from "../../components/menu/MenuSelectLanguage";
import {generateUrl} from "../../utils/routing";

const useStyles = makeStyles(theme => ({
    grow: {
        flex: "1 1 auto",
    },
    app_name: {
        marginRight: theme.spacing(2),
        textDecoration: "none"
    },
    icon: {
        color: theme.palette.primary.contrastText,
        cursor: "pointer",
    },
}));

const TopDesktop = ({actions, config}) => {
    const intl = useIntl();
    const classes = useStyles();

    return (
        <AppBar position="relative">
            <Toolbar>
                <Typography variant="h6" color="inherit" noWrap component={Link}
                            to={generateUrl("", config.locale)}
                            className={classes.app_name}
                >
                    {intl.formatMessage({id: "app_name"})}
                </Typography>
                <Button color="inherit" aria-label={intl.formatMessage({id: "menu.routines"})} component={Link}
                        to={generateUrl("/routines", config.locale)}>
                    {intl.formatMessage({id: "menu.routines"})}
                </Button>
                <Button color="inherit" aria-label={intl.formatMessage({id: "menu.exercises"})} component={Link}
                        to={generateUrl("/exercises", config.locale)}>
                    {intl.formatMessage({id: "menu.exercises"})}
                </Button>
                <Button color="inherit" aria-label={intl.formatMessage({id: "menu.relaxation"})} component={Link}
                        to={generateUrl("/relaxation", config.locale)}>
                    {intl.formatMessage({id: "menu.relaxation"})}
                </Button>
                <Button color="inherit" aria-label={intl.formatMessage({id: "menu.skills"})}
                        component={Link} to={generateUrl("/skills", config.locale)}>
                    {intl.formatMessage({id: "menu.skills"})}
                </Button>

                <div className={classes.grow} />

                <Tooltip title={intl.formatMessage({id: "menu.account"})} enterDelay={300}>
                    <IconButton color="inherit" aria-label={intl.formatMessage({id: "menu.account"})}
                                component={Link} to={generateUrl("/account", config.locale)}>
                        <ProfileIcon />
                    </IconButton>
                </Tooltip>
                <Tooltip title={intl.formatMessage({id: "menu.change_theme"})} enterDelay={300}>
                    <IconButton
                        onClick={() => {
                            actions.setTheme(config.theme === "light" ? "dark" : "light");
                        }}
                        aria-label={intl.formatMessage({id: "menu.change_theme"})}
                    >
                        {config.theme === "light" && (
                            <Brightness4Icon classes={{root: classes.icon}} />
                        )}
                        {config.theme === "dark" && (
                            <BrightnessHighIcon classes={{root: classes.icon}} />
                        )}
                    </IconButton>
                </Tooltip>

                <MenuSelectLanguage locale={config.locale} />
            </Toolbar>
        </AppBar>
    );
};

TopDesktop.propTypes = {
    actions: PropTypes.shape({
        setFont: PropTypes.func,
        setLocale: PropTypes.func,
        setTheme: PropTypes.func
    }).isRequired,
    config: PropTypes.shape({
        font: PropTypes.string,
        locale: PropTypes.string,
        theme: PropTypes.string
    }).isRequired
};

export default TopDesktop;