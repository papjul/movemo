import CssBaseline from "@material-ui/core/CssBaseline";
import {ThemeProvider} from "@material-ui/core/styles";
import Intl from "intl";
import PropTypes from "prop-types";
import React from "react";
import {IntlProvider} from "react-intl";
import {connect} from "react-redux";
import {Redirect, Route, Switch} from "react-router-dom";
import {bindActionCreators} from "redux";
import Login from "../pages/account/Login";
import PageNotFound from "../pages/error/NotFound";
import * as movemoActionCreators from "../redux/actions";
import {privateRoutes, publicRoutes} from "../routes";
import locales from "../translations";
import {getLocaleMessages} from "../utils/locale";
import {generateUrl} from "../utils/routing";
import getThemeSource from "../utils/theme";

global.Intl = Intl;

const RootContainer = ({config, dispatch, entities}) => {
    const actions = bindActionCreators(movemoActionCreators, dispatch);
    const locale = config.locale || "en"; // TODO: Redundant with store definition?
    const theme = config.theme || "light"; // TODO: Redundant with store definition?
    const font = config.font || "default"; // TODO: Redundant with store definition?

    return (
        <IntlProvider locale={locale} key={locale} messages={getLocaleMessages(locale, locales)}>
            <ThemeProvider theme={getThemeSource(theme, locale, font)}>
                <CssBaseline />
                <Switch>
                    {/* Authorization must be in the render function to avoid early redirect */}
                    <Route path="/:locale(en|fr)/login" exact
                           render={() => !config.authToken
                               ? <Login {...{actions, config}} />
                               : <Redirect to={generateUrl("", locale)} />
                           }
                    />

                    {privateRoutes.map((route, idx) => {
                            const Component = route.component;
                            return (
                                <Route key={idx} path={route.path} exact
                                       render={() => config.authToken
                                           ? <Component {...{actions, config, entities}} />
                                           : <Redirect to={generateUrl("/login", locale)} />
                                       }
                                />
                            );
                        }
                    )}

                    {publicRoutes.map((route, idx) => {
                            const Component = route.component;
                            return (
                                <Route key={idx} path={route.path} exact
                                       render={() => <Component {...{actions, config, entities}} />}
                                />
                            );
                        }
                    )}

                    <Route status={404} render={() => <PageNotFound {...{actions, config}} />} />
                </Switch>
            </ThemeProvider>
        </IntlProvider>
    );
};

RootContainer.propTypes = {
    config: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    entities: PropTypes.object.isRequired
};

function select(state) {
    // Which part of the Redux global state does our component want to receive as props?
    return {
        config: state.config,
        entities: state.entities,
    };
}

export default connect(select)(RootContainer);
