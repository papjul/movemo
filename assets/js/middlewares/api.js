import {camelizeKeys} from "humps";
import {omit} from "lodash";
import {normalize, schema} from "normalizr";
import ReactOnRails from "react-on-rails";
import {generateUrl} from "../utils/routing";

// Extracts the next page URL
const getNextPageUrl = response => {
    const link = response.headers.get("link");
    if (!link) {
        return null;
    }

    const nextLink = link.split(",").find(s => s.indexOf("rel=\"next\"") > -1);
    if (!nextLink) {
        return null;
    }

    return nextLink.trim().split(";")[0].slice(1, -1);
};

const API_ROOT = "/api/";

// Fetches an API response and normalizes the result JSON according to schema.
// This makes every API response have the same shape, regardless of how nested it was.
const callApi = (endpoint, schema, locale, token) => {
    const fullUrl = generateUrl(API_ROOT + endpoint, locale);
    let parameters = {};
    if (token) {
        parameters = {
            headers: ReactOnRails.authenticityHeaders({
                Authorization: "Bearer " + token
            })
        };
    }

    return fetch(fullUrl, parameters)
        .then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json);
                }

                const camelizedJson = camelizeKeys(json);
                const nextPageUrl = getNextPageUrl(response);

                return Object.assign({},
                    normalize(camelizedJson, schema),
                    {nextPageUrl}
                );
            })
        );
};

// We use this Normalizr schemas to transform API responses from a nested form
// to a flat form where entities are placed in `entities`, and nested
// JSON objects are replaced with their IDs. This is very convenient for
// consumption by reducers, because we can easily build a normalized tree
// and keep it updated as we fetch more data.

// Read more about Normalizr: https://github.com/paularmstrong/normalizr

// API may return results with uppercase letters while the query
// doesn't contain any. For example, "someuser" could result in "SomeUser"
// leading to a frozen UI as it wouldn't find "someuser" in the entities.
// That's why we're forcing lower cases down there.

// TODO: Add merge/fallback strategy for translations (fr -> en if no fr translation)
const featuredRoutineSchema = new schema.Entity("featuredRoutines", {}, {
    idAttribute: featuredRoutine => featuredRoutine.nameId.toLowerCase()
});

const routineSchema = new schema.Entity("routines", {}, {
    idAttribute: routine => routine.nameId.toLowerCase(),
    processStrategy: routine => {
        if ('items' in routine) {
            routine.items.forEach(item => {
                item.isDuration = item.duration;
                omit(item, 'duration')
            })
        }
        return routine;
    }
});

const routineCategorySchema = new schema.Entity("routineCategories", {
    routines: [routineSchema]
}, {
    idAttribute: routineCategory => routineCategory.id
});

const exerciseSchema = new schema.Entity("exercises", {}, {
    idAttribute: exercise => exercise.nameId.toLowerCase()
});

const progressionExerciseSchema = new schema.Entity("progressionExercises", {}, {
    idAttribute: progressionExercise => progressionExercise.nameId.toLowerCase()
});

const skillSchema = new schema.Entity("skills", {}, {
    idAttribute: skill => skill.source.nameId.toLowerCase() + '-' + skill.target.nameId.toLowerCase()
});

// Schemas for API responses.
export const Schemas = {
    FEATURED_ROUTINE: featuredRoutineSchema, // TODO: Find a way to merge with routines
    FEATURED_ROUTINE_ARRAY: [featuredRoutineSchema], // TODO: Find a way to merge with routines
    ROUTINE_CATEGORY: routineCategorySchema,
    ROUTINE_CATEGORY_ARRAY: [routineCategorySchema],
    ROUTINE: routineSchema,
    ROUTINE_ARRAY: [routineSchema],
    EXERCISE: exerciseSchema,
    EXERCISE_ARRAY: [exerciseSchema],
    PROGRESSION_EXERCISE: progressionExerciseSchema,
    PROGRESSION_EXERCISE_ARRAY: [progressionExerciseSchema],
    SKILL: skillSchema,
    SKILL_ARRAY: [skillSchema],
};

// Action key that carries API call info interpreted by this Redux middleware.
export const CALL_API = "Call API";

// A Redux middleware that interprets actions with CALL_API info specified.
// Performs the call and promises when such actions are dispatched.
export default store => next => action => {
    const callAPI = action[CALL_API];
    if (typeof callAPI === "undefined") {
        return next(action);
    }

    let {endpoint} = callAPI;
    const {locale, schema, token, types} = callAPI;

    if (typeof endpoint === "function") {
        endpoint = endpoint(store.getState());
    }

    if (typeof endpoint !== "string") {
        throw new Error("Specify a string endpoint URL.");
    }
    if (!schema) {
        throw new Error("Specify one of the exported Schemas.");
    }
    if (!Array.isArray(types) || types.length !== 3) {
        throw new Error("Expected an array of three action types.");
    }
    if (!types.every(type => typeof type === "string")) {
        throw new Error("Expected action types to be strings.");
    }

    const actionWith = data => {
        const finalAction = Object.assign({}, action, data);
        delete finalAction[CALL_API];
        return finalAction;
    };

    const [requestType, successType, failureType] = types;
    next(actionWith({type: requestType}));

    return callApi(endpoint, schema, locale, token).then(
        response => next(actionWith({
            response,
            type: successType
        })),
        error => next(actionWith({
            type: failureType,
            error: error.message || "Something bad happened"
        }))
    );
}
