import jwtDecode from "jwt-decode";

import {CALL_API, Schemas} from "../middlewares/api";
import {generateUrl} from "../utils/routing";

export const FEATURED_ROUTINES_REQUEST = "FEATURED_ROUTINES_REQUEST";
export const FEATURED_ROUTINES_SUCCESS = "FEATURED_ROUTINES_SUCCESS";
export const FEATURED_ROUTINES_FAILURE = "FEATURED_ROUTINES_FAILURE";

export const fetchFeaturedRoutines = (locale, token) => ({
    [CALL_API]: {
        types: [FEATURED_ROUTINES_REQUEST, FEATURED_ROUTINES_SUCCESS, FEATURED_ROUTINES_FAILURE],
        endpoint: "featured-routines",
        schema: Schemas.FEATURED_ROUTINE_ARRAY,
        locale,
        token
    }
});

export const ROUTINE_CATEGORIES_REQUEST = "ROUTINE_CATEGORIES_REQUEST";
export const ROUTINE_CATEGORIES_SUCCESS = "ROUTINE_CATEGORIES_SUCCESS";
export const ROUTINE_CATEGORIES_FAILURE = "ROUTINE_CATEGORIES_FAILURE";

export const fetchRoutineCategories = (locale, token) => ({
    [CALL_API]: {
        types: [ROUTINE_CATEGORIES_REQUEST, ROUTINE_CATEGORIES_SUCCESS, ROUTINE_CATEGORIES_FAILURE],
        endpoint: "routine-categories",
        schema: Schemas.ROUTINE_CATEGORY_ARRAY,
        locale,
        token
    }
});

export const ROUTINE_REQUEST = "ROUTINE_REQUEST";
export const ROUTINE_SUCCESS = "ROUTINE_SUCCESS";
export const ROUTINE_FAILURE = "ROUTINE_FAILURE";

export const fetchRoutine = (locale, token, name_id) => ({
    [CALL_API]: {
        types: [ROUTINE_REQUEST, ROUTINE_SUCCESS, ROUTINE_FAILURE],
        endpoint: "routines/" + name_id,
        schema: Schemas.ROUTINE,
        locale,
        token
    }
});

export const EXERCISES_REQUEST = "EXERCISES_REQUEST";
export const EXERCISES_SUCCESS = "EXERCISES_SUCCESS";
export const EXERCISES_FAILURE = "EXERCISES_FAILURE";

export const fetchExercises = (locale, token) => ({
    [CALL_API]: {
        types: [EXERCISES_REQUEST, EXERCISES_SUCCESS, EXERCISES_FAILURE],
        endpoint: "exercises",
        schema: Schemas.EXERCISE_ARRAY,
        locale,
        token
    }
});

export const EXERCISE_REQUEST = "EXERCISE_REQUEST";
export const EXERCISE_SUCCESS = "EXERCISE_SUCCESS";
export const EXERCISE_FAILURE = "EXERCISE_FAILURE";

export const fetchExercise = (locale, token, name_id) => ({
    [CALL_API]: {
        types: [EXERCISE_REQUEST, EXERCISE_SUCCESS, EXERCISE_FAILURE],
        endpoint: "exercises/" + name_id,
        schema: Schemas.EXERCISE,
        locale,
        token
    }
});

export const PROGRESSION_EXERCISES_REQUEST = "PROGRESSION_EXERCISES_REQUEST";
export const PROGRESSION_EXERCISES_SUCCESS = "PROGRESSION_EXERCISES_SUCCESS";
export const PROGRESSION_EXERCISES_FAILURE = "PROGRESSION_EXERCISES_FAILURE";

export const fetchProgressionExercises = (locale, token) => ({
    [CALL_API]: {
        types: [PROGRESSION_EXERCISES_REQUEST, PROGRESSION_EXERCISES_SUCCESS, PROGRESSION_EXERCISES_FAILURE],
        endpoint: "progression-exercises",
        schema: Schemas.PROGRESSION_EXERCISE_ARRAY,
        locale,
        token
    }
});

export const SKILLS_REQUEST = "SKILLS_REQUEST";
export const SKILLS_SUCCESS = "SKILLS_SUCCESS";
export const SKILLS_FAILURE = "SKILLS_FAILURE";

export const fetchSkills = (locale, token) => ({
    [CALL_API]: {
        types: [SKILLS_REQUEST, SKILLS_SUCCESS, SKILLS_FAILURE],
        endpoint: "skills",
        schema: Schemas.SKILL_ARRAY,
        locale,
        token
    }
});

// Old API calls
export const SET_IS_FETCHING = "SET_IS_FETCHING";

export const setIsFetching = () => ({
    type: SET_IS_FETCHING,
});

export const login = (locale, username, password) => (dispatch) => {
    dispatch(setIsFetching());
    let data = new FormData();
    data.append("_username", username);
    data.append("_password", password);

    return (
        fetch(generateUrl("/api/login_check", "en"), {
            method: "POST",
            body: data
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw Error(response.statusText);
                }
            })
            .then(data => {
                const payload = jwtDecode(data.token);
                let d = new Date(payload.exp * 1000);
                document.cookie =
                    "BEARER=" + data.token + "; expires=" + d.toUTCString() + "; path=/";
                dispatch(loginSuccess(data.token));
            })
            .catch(() => {
                dispatch(loginFailure());
            })
    );
};

export const LOGIN_TOKEN_RECEIVED = "LOGIN_TOKEN_RECEIVED";

export const loginSuccess = (token) => ({
    type: LOGIN_TOKEN_RECEIVED,
    authToken: token
}); // TODO: Dispatch locale, theme and font

export const LOGIN_ERROR = "LOGIN_ERROR";

export const loginFailure = () => ({
    type: LOGIN_ERROR
});

export const SET_LOCALE = "SET_LOCALE";

export const setLocale = (locale) => ({
    type: SET_LOCALE,
    locale,
});

export const SET_THEME = "SET_THEME";

export const setTheme = (theme) => ({
    type: SET_THEME,
    theme,
});

export const SET_FONT = "SET_FONT";

export const setFont = (font) => ({
    type: SET_FONT,
    font,
});

// Resets the currently visible error message.
export const RESET_ERROR_MESSAGE = "RESET_ERROR_MESSAGE";

export const resetErrorMessage = () => ({
    type: RESET_ERROR_MESSAGE
});