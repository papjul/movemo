import {camelizeKeys} from "humps";
import {normalize} from "normalizr";
import {applyMiddleware, compose, createStore} from "redux";
import {createLogger} from "redux-logger";
import thunk from "redux-thunk";
import DevTools from "../containers/DevTools";
import api, {Schemas} from "../middlewares/api";
import reducers from "./reducers";

export default (props, context) => {
    // Data
    const initialFeaturedRoutines = props.featuredRoutines ? normalize(camelizeKeys(props.featuredRoutines), Schemas.FEATURED_ROUTINE_ARRAY).entities.featuredRoutines : {};
    const initialRoutineCategories = props.routineCategories ? normalize(camelizeKeys(props.routineCategories), Schemas.ROUTINE_CATEGORY_ARRAY).entities.routineCategories : {};
    const initialRoutines = props.routineCategories
        ? normalize(camelizeKeys(props.routineCategories), Schemas.ROUTINE_CATEGORY_ARRAY).entities.routines
        : (props.routine ? normalize(camelizeKeys(props.routine), Schemas.ROUTINE).entities.routines : {});
    const initialExercises = props.exercises
        ? normalize(camelizeKeys(props.exercises), Schemas.EXERCISE_ARRAY).entities.exercises
        : (props.exercise ? normalize(camelizeKeys(props.exercise), Schemas.EXERCISE).entities.exercises : {});
    const initialProgressionExercises = props.progressionExercises ? normalize(camelizeKeys(props.progressionExercises), Schemas.PROGRESSION_EXERCISE_ARRAY).entities.progressionExercises : {};
    const initialSkills = props.skills ? normalize(camelizeKeys(props.skills), Schemas.SKILL_ARRAY).entities.skills : {};

    // Form
    //const initialChangePasswordForm = props.changePasswordForm;

    // Settings
    const initialLocale = props.locale || "en";
    const initialTheme = props.theme || "light";
    const initialFont = props.font || "default";
    const initialAuthToken = props.authToken;

    const initialState = {
        config: {
//            $$changePasswordForm: initialChangePasswordForm,
            isFetching: false,
            authToken: initialAuthToken,
            locale: initialLocale,
            theme: initialTheme,
            font: initialFont,
        },
        entities: {
            featuredRoutines: initialFeaturedRoutines,
            routineCategories: initialRoutineCategories,
            routines: initialRoutines,
            progressionExercises: initialProgressionExercises,
            exercises: initialExercises,
            skills: initialSkills
        }
    };

    //const reducer = combineReducers(reducers);
    let composedStore;
    if (process.env.NODE_ENV !== "development") {
        composedStore = compose(
            applyMiddleware(thunk, api),
        );
    } else {
        composedStore = compose(
            applyMiddleware(thunk, api, createLogger()),
            DevTools.instrument()
        );
    }

    return composedStore(createStore)(reducers, initialState);
};