import {LOGIN_ERROR, LOGIN_TOKEN_RECEIVED, SET_FONT, SET_IS_FETCHING, SET_LOCALE, SET_THEME} from "../actions";

export const initialConfig = {
    isFetching: false,
    authToken: null,
    locale: null,
    theme: null,
    font: false,
};

// TODO: Add function to reset state
const configReducer = (state = initialConfig, action = null) => {
    const {type, authToken, locale, theme, font} = action;

    switch (type) {
        case LOGIN_TOKEN_RECEIVED:
            return {
                ...state,
                authToken,
                isFetching: false
            };

        case LOGIN_ERROR:
            return {
                ...state,
                isFetching: false
            };

        case SET_IS_FETCHING:
            return {
                ...state,
                isFetching: true
            };

        case SET_LOCALE:
            return {
                ...state,
                locale
            };

        case SET_THEME:
            return {
                ...state,
                theme
            };

        case SET_FONT:
            return {
                ...state,
                font
            };

        default:
            return state;
    }
};

export default configReducer;
