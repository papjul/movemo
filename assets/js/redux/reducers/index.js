import merge from "lodash/merge";
import {combineReducers} from "redux";
import {RESET_ERROR_MESSAGE} from "../actions";
import configReducer, {initialConfig} from "./configReducer";

const initialEntities = {
    featuredRoutines: {},
    routines: {},
    routine: {},
    routineItems: {},
    progressionExercises: {},
    exercises: {},
    exercise: {},
    skills: {},
};

// Updates an entity cache in response to any action with response.entities.
const entities = (state = initialEntities, action) => {
    if (action.response && action.response.entities) {
        return merge({}, state, action.response.entities);
    }

    return state;
};

// Updates error message to notify about the failed fetches.
const errorMessage = (state = null, action) => {
    const {type, error} = action;

    if (type === RESET_ERROR_MESSAGE) {
        return null;
    } else if (error) {
        return error;
    }

    return state;
};

export default combineReducers({
    config: configReducer,
    entities,
    errorMessage,
});

export const initialStates = {
    config: initialConfig,
    entities: initialEntities
};
