import {ServerStyleSheets} from "@material-ui/core/styles";
import React from "react";
import ReactDOMServer from "react-dom/server";
import {Helmet} from "react-helmet";
import ReactOnRails from "react-on-rails";
import {Provider} from "react-redux";
import {StaticRouter} from "react-router";
import {BrowserRouter} from "react-router-dom";
import RootContainer from "./containers/Root";

// See documentation for https://github.com/reactjs/react-redux.
// This code here binds your smart component to the redux store.
export default (_initialProps, context) => {
    const store = ReactOnRails.getStore("MovemoStore");
    const {location, base, serverSide} = context;

    // We render a different router depending on whether we are rendering server side
    // or client side.
    let Router;
    /*if (serverSide) {
        Router = ({children}) => (
            <StaticRouter basename={base} location={location} context={{}}>
                {children}
            </StaticRouter>
        );
    } else {*/
        Router = ({children}) => (
            <BrowserRouter basename={base}>
                {children}
            </BrowserRouter>
        );
    //}

    const reactComponent = (
        <React.StrictMode>
            <Provider store={store}>
                <Router>
                    <RootContainer />
                </Router>
            </Provider>
        </React.StrictMode>
    );

    /*if (serverSide) {
        const sheets = new ServerStyleSheets();

        const renderedHtml = {
            componentHtml: ReactDOMServer.renderToString(
                sheets.collect(reactComponent),
            ),
            componentStyles: sheets.toString(),
            title: Helmet.renderStatic().title.toString(),
        };
        return {renderedHtml};
    }*/

    return reactComponent;
};
