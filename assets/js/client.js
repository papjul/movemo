import ReactOnRails from "react-on-rails";
import "whatwg-fetch"; // Polyfill for fetch()
import "../scss/routine-run.scss"; // TODO: Remove, replace with makeStyles
import MovemoApp from "./app";
import MovemoStore from "./redux/store";

import '@formatjs/intl-locale/polyfill'

import '@formatjs/intl-numberformat/polyfill'
import '@formatjs/intl-numberformat/locale-data/en' // locale-data for en
import '@formatjs/intl-numberformat/locale-data/fr' // locale-data for fr

//import '@formatjs/intl-datetimeformat/polyfill'
//import '@formatjs/intl-datetimeformat/locale-data/en' // locale-data for en
//import '@formatjs/intl-datetimeformat/locale-data/fr' // locale-data for fr
//import '@formatjs/intl-datetimeformat/add-all-tz' // Add ALL tz data

/*if (!Intl.PluralRules) {
    require("@formatjs/intl-pluralrules/polyfill");
    require("@formatjs/intl-pluralrules/locale-data/fr"); // Add locale data for fr
}*/

import '@formatjs/intl-pluralrules/polyfill'
import '@formatjs/intl-pluralrules/locale-data/en' // locale-data for en
import '@formatjs/intl-pluralrules/locale-data/fr' // locale-data for fr

/*if (!Intl.RelativeTimeFormat) {
    require("@formatjs/intl-relativetimeformat/polyfill");
    require("@formatjs/intl-relativetimeformat/locale-data/fr"); // Add locale data for fr
}*/
import '@formatjs/intl-relativetimeformat/polyfill'
import '@formatjs/intl-relativetimeformat/locale-data/en' // locale-data for en
import '@formatjs/intl-relativetimeformat/locale-data/fr' // locale-data for fr

ReactOnRails.register({MovemoApp});
ReactOnRails.registerStore({MovemoStore});

