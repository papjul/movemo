import dagre from "cytoscape-dagre/cytoscape-dagre.js";
import cytoscape from "cytoscape/dist/cytoscape.min.js";
import "../scss/progression.scss";

cytoscape.use(dagre);

window.addEventListener("DOMContentLoaded", function () {
    var cy = cytoscape({
        container: document.getElementById("progressionGraph"),

        userPanningEnabled: false,
        boxSelectionEnabled: false,
        //autounselectify: true,

        style: styles,

        elements: {
            nodes: nodes,
            edges: edges
        },

        layout: {
            name: "dagre",
            //nodeDimensionsIncludeLabels: true,
            rankDir: "LR",
            ranker: "network-simplex", // 'network-simplex', 'tight-tree' or 'longest-path'
            //directed: true,
            //padding: 10,
            //grid: true,
            //avoidOverlap: true
        }
    });
});