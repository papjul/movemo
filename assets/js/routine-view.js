function updateProgressionChoice(el) {
    var selectedIndex = el.target.selectedIndex;
    var sameProgressionItems = document.getElementsByClassName(el.target.classList[2]);
    for (var i = 0; i < sameProgressionItems.length; ++i) {
        sameProgressionItems[i].selectedIndex = selectedIndex;
        // TODO: Update image
    }
}

var progressionItems = document.getElementsByClassName("progression");

for (var i = 0; i < progressionItems.length; ++i) {
    progressionItems[i].addEventListener("change", updateProgressionChoice);
}