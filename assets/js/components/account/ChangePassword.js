import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";
import {useIntl} from "react-intl";

const useStyles = makeStyles((theme) => ({
    paper: {
        minHeight: "calc(100vh - 56px - 56px - " + theme.spacing(6) + ")",
        height: "calc(100vh - 56px - 56px - " + theme.spacing(6) + ")",
        [`${theme.breakpoints.up("xs")} and (orientation: landscape)`]: {
            minHeight: "calc(100vh - 48px - 56px - " + theme.spacing(6) + ")",
            height: "calc(100vh - 48px - 56px - " + theme.spacing(6) + ")",
        },
        [theme.breakpoints.up("sm")]: {
            minHeight: "calc(100vh - 64px - 56px - " + theme.spacing(6) + ") !important",
            height: "calc(100vh - 64px - 56px - " + theme.spacing(6) + ") !important",
        },
        [theme.breakpoints.up("lg")]: {
            minHeight: "calc(100vh - 56px - 82px - " + theme.spacing(6) + ")",
            height: "calc(100vh - 56px - 82px - " + theme.spacing(6) + ")",
        },
    },
    form: {
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    container: {
        width: "auto",
        marginLeft: "auto",
        marginRight: "auto",
        padding: `${theme.spacing(2)} ${theme.spacing(3)} ${theme.spacing(3)}`,
    },
}));

const ChangePassword = ({actions, config}) => {
    const classes = useStyles();
    const intl = useIntl();
    /*    const [username, setUsername] = useState('')
        const [password, setPassword] = useState('')
        const [showPassword, setShowPassword] = useState(false);*/

    /*const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };*/

    function handleSubmit(event) {
        event.preventDefault();
        //const {login} = actions;
        //login(data.get('locale'), username, password);
    }

    return (
        <Grid container className={classes.paper} alignItems="center">
            <Grid item className={classes.container} component={Paper} elevation={6}>
                <Typography component="h1" variant="h5" align="center">
                    {intl.formatMessage({id: "title.change_password"})}
                </Typography>
                <p>Page à venir.</p>
                {/*<Liform
                        schema={JSON.stringify(data.get('$$changePasswordForm'))}
                        //onSubmit={submit.bind(this, baseUrl, authToken)}
                        //initialValues={initialValues}
                    />
                    <form className={classes.form} onSubmit={handleSubmit} noValidate method="POST">
                        <TextField
                            value={username}
                            onInput={(e) => setUsername(e.target.value)}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="username"
                            label={intl.formatMessage({id: 'login.username'})}
                            name="username"
                            autoComplete="username"
                            autoFocus
                        />
                        <FormControl variant="outlined" fullWidth>
                            <InputLabel htmlFor="password">{intl.formatMessage({id: 'login.password'})}</InputLabel>
                            <OutlinedInput
                                value={password}
                                onInput={(e) => setPassword(e.target.value)}
                                required
                                name="password"
                                label={intl.formatMessage({id: 'login.password'})}
                                type={showPassword ? 'text' : 'password'}
                                id="password"
                                autoComplete="current-password"
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={() => setShowPassword(!showPassword)}
                                            onMouseDown={handleMouseDownPassword}
                                        >
                                            {showPassword ? <Visibility/> : <VisibilityOff/>}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            aria-label={intl.formatMessage({id: 'login.submit'})}
                            disabled={data.get('isFetching')}
                        >
                            {intl.formatMessage({id: 'login.submit'})} {data.get('isFetching') &&
                        <CircularProgress size={14} style={{marginLeft: 14}}/>}
                        </Button>
                    </form>

                    <div
                        style={{
                            display: 'flex',
                            flexDirection: 'row',
                            width: '100%',
                            justifyContent: 'space-between',
                        }}
                    >
                        <MuiLink
                            to={generateUrl("/password_reset", data.get('locale'))}
                            component={Link}>{intl.formatMessage({id: 'login.forgot_password'})}</MuiLink>
                        <MuiLink
                            to={generateUrl("/register", data.get('locale'))}
                            component={Link}>{intl.formatMessage({id: 'login.register'})}</MuiLink>
                    </div>*/}
            </Grid>
        </Grid>
    );
};

export default ChangePassword;