import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import PropTypes from "prop-types";
import React from "react";
import {useIntl} from "react-intl";

const MenuSettingsTheme = ({onClose, selectedValue, open}) => {
    const intl = useIntl();

    const handleThemeClose = () => {
        onClose(selectedValue);
    };

    const handleThemeListItemClick = (event) => {
        onClose(event.target.value);
    };

    return (
        <Dialog onClose={handleThemeClose} aria-labelledby="menu-settings-theme" open={open} fullWidth>
            <DialogTitle id="menu-settings-theme">{intl.formatMessage({id: "settings.theme"})}</DialogTitle>
            <DialogContent>
                <RadioGroup aria-label={intl.formatMessage({id: "settings.theme"})} name="theme" value={selectedValue}
                            onChange={handleThemeListItemClick}>
                    <FormControlLabel key="light" value="light" control={<Radio />}
                                      label={intl.formatMessage({id: "settings.theme.light"})} />
                    <FormControlLabel key="dark" value="dark" control={<Radio />}
                                      label={intl.formatMessage({id: "settings.theme.dark"})} />
                </RadioGroup>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleThemeClose} color="primary">
                    {intl.formatMessage({id: "label.cancel"})}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

MenuSettingsTheme.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    selectedValue: PropTypes.string.isRequired
};

export default MenuSettingsTheme;