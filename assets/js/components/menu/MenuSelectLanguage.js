import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import NoSsr from "@material-ui/core/NoSsr";
import Tooltip from "@material-ui/core/Tooltip";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import LanguageIcon from "@material-ui/icons/Translate";
import React, {useState} from "react";
import {useIntl} from "react-intl";
import {Link, useLocation} from "react-router-dom";
import {LANGUAGES_LABEL} from "../../constants";
import {pathnameToLanguage} from "../../utils/routing";

const MenuSelectLanguage = ({locale}) => {
    const [languageMenu, setLanguageMenu] = useState(null);
    const handleLanguageIconClick = (event) => {
        setLanguageMenu(event.currentTarget);
    };
    const handleLanguageMenuClose = (event) => {
        setLanguageMenu(null);
    };

    const location = useLocation();
    const {canonical} = pathnameToLanguage(location.pathname);

    const intl = useIntl();

    return (
        <>
            <Tooltip title={intl.formatMessage({id: "menu.change_language"})} enterDelay={300}>
                <IconButton
                    color="inherit"
                    aria-owns={languageMenu ? "language-menu" : undefined}
                    aria-haspopup="true"
                    aria-label={intl.formatMessage({id: "menu.change_language"})}
                    onClick={handleLanguageIconClick}
                    data-ga-event-category="header"
                    data-ga-event-action="language"
                    edge="end"
                    style={{
                        borderRadius: 4
                    }}
                >
                    <LanguageIcon />
                    <span>{/* FIXME className={classes.language}*/}
                        {LANGUAGES_LABEL.filter((language) => language.code === locale).text}
                  </span>
                    <ExpandMoreIcon fontSize="small" />
                </IconButton>
            </Tooltip>
            <NoSsr defer>
                <Menu
                    id="language-menu"
                    anchorEl={languageMenu}
                    open={Boolean(languageMenu)}
                    onClose={handleLanguageMenuClose}
                >
                    {LANGUAGES_LABEL.map((language) => (
                        <MenuItem
                            component={Link}
                            data-no-link="true"
                            to={`/${language.code}${canonical}`}
                            key={language.code}
                            selected={locale === language.code}
                            onClick={handleLanguageMenuClose}
                            lang={language.code}
                            hrefLang={language.code}
                        >
                            {language.text}
                        </MenuItem>
                    ))}
                </Menu>
            </NoSsr>
        </>
    );
};

// We should do export default SelectLanguage but there is a bug with Hooks in React on Rails. Should hopefully be fixed with v12
export default MenuSelectLanguage;