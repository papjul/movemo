import AppBar from "@material-ui/core/AppBar";
import Dialog from "@material-ui/core/Dialog";
import Divider from "@material-ui/core/Divider";
import Fab from "@material-ui/core/Fab";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import Slide from "@material-ui/core/Slide";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Brightness4Icon from "@material-ui/icons/Brightness4";
import BrightnessHighIcon from "@material-ui/icons/BrightnessHigh";
import CloseIcon from "@material-ui/icons/Close";
import ApplyIcon from "@material-ui/icons/Done";
import FontIcon from "@material-ui/icons/FontDownload";
import SettingsIcon from "@material-ui/icons/Settings";
import LanguageIcon from "@material-ui/icons/Translate";
import React, {forwardRef, useState} from "react";
import {useIntl} from "react-intl";
import {LANGUAGES_LABEL} from "../../constants";
import MenuSettingsFont from "./MenuSettingsFont";
import MenuSettingsLanguage from "./MenuSettingsLanguage";
import MenuSettingsTheme from "./MenuSettingsTheme";

const useStyles = makeStyles((theme) => ({
    appBar: {
        position: "relative",
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
    icon: {
        color: theme.palette.primary.contrastText,
        cursor: "pointer",
    },
    fab: {
        position: "fixed",
        bottom: theme.spacing(2),
        right: theme.spacing(2),
    },
}));

const Transition = forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const MenuSettings = ({actions, config}) => {
    const classes = useStyles();
    const intl = useIntl();

    const [settingsOpen, setSettingsOpen] = useState(false);
    const handleClickSettingsOpen = () => {
        setSettingsOpen(true);
    };
    const handleSettingsClose = () => {
        setSettingsOpen(false);
        setApplyIcon(false);
    };

    const [applyIcon, setApplyIcon] = useState(false);

    const [languageOpen, setLanguageOpen] = useState(false);
    const handleLanguageClickOpen = () => {
        setLanguageOpen(true);
    };
    const handleLanguageClose = (value) => {
        setLanguageOpen(false);
        if (value !== config.locale) {
            //actions.setLocale(value);
            setApplyIcon(true);
        }
    };

    const getLanguageLabel = () => {
        for (let language of LANGUAGES_LABEL) {
            if (config.locale === language.code) {
                return language.text;
            }
        }
        return null;
    };

    const [themeOpen, setThemeOpen] = useState(false);
    const handleThemeClickOpen = () => {
        setThemeOpen(true);
    };
    const handleThemeClose = (value) => {
        setThemeOpen(false);
        if (value !== config.theme) {
            actions.setTheme(value);
            setApplyIcon(true);
        }
    };

    const [fontOpen, setFontOpen] = useState(false);
    const handleFontClickOpen = () => {
        setFontOpen(true);
    };
    const handleFontClose = (value) => {
        setFontOpen(false);
        if (value !== config.font) {
            actions.setFont(value);
            setApplyIcon(true);
        }
    };

    return (
        <>
            <IconButton onClick={handleClickSettingsOpen} className={classes.icon}
                        aria-label={intl.formatMessage({id: "menu.open_settings"})} edge="end">
                <SettingsIcon />
            </IconButton>
            <Dialog fullScreen open={settingsOpen} onClose={handleSettingsClose} TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton edge="start" color="inherit" onClick={handleSettingsClose}
                                    aria-label={intl.formatMessage({id: applyIcon ? "label.apply" : "label.close"})}>
                            {applyIcon ? <ApplyIcon /> : <CloseIcon />}
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                            {intl.formatMessage({id: "title.settings"})}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <List subheader={<ListSubheader>{intl.formatMessage({id: "settings.general"})}</ListSubheader>}>
                    <ListItem button onClick={handleLanguageClickOpen}>
                        <ListItemIcon>
                            <LanguageIcon />
                        </ListItemIcon>
                        <ListItemText primary={intl.formatMessage({id: "settings.language"})}
                                      secondary={getLanguageLabel()} />
                    </ListItem>
                    <Divider />
                    <ListItem button onClick={handleThemeClickOpen}>
                        <ListItemIcon>
                            {config.theme === "light" && (
                                <Brightness4Icon />
                            )}
                            {config.theme === "dark" && (
                                <BrightnessHighIcon />
                            )}
                        </ListItemIcon>
                        <ListItemText primary={intl.formatMessage({id: "settings.theme"})}
                                      secondary={intl.formatMessage({id: "settings.theme." + config.theme})} />
                    </ListItem>
                </List>
                <List subheader={<ListSubheader>{intl.formatMessage({id: "settings.accessibility"})}</ListSubheader>}>
                    <ListItem button onClick={handleFontClickOpen}>
                        <ListItemIcon>
                            <FontIcon />
                        </ListItemIcon>
                        <ListItemText primary={intl.formatMessage({id: "settings.font"})}
                                      secondary={intl.formatMessage({id: "settings.font." + config.font})} />
                    </ListItem>
                </List>
                <MenuSettingsLanguage selectedValue={config.locale} open={languageOpen}
                                      onClose={handleLanguageClose} />
                <MenuSettingsTheme selectedValue={config.theme} open={themeOpen} onClose={handleThemeClose} />
                <MenuSettingsFont selectedValue={config.font} open={fontOpen} onClose={handleFontClose} />
                {applyIcon &&
                <Fab color="primary" aria-label={intl.formatMessage({id: "label.apply"})} className={classes.fab}
                     onClick={handleSettingsClose}>
                    <ApplyIcon />
                </Fab>}
            </Dialog>
        </>
    );
};

export default MenuSettings;