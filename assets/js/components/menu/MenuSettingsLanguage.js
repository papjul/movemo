import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import PropTypes from "prop-types";
import React from "react";
import {useIntl} from "react-intl";
import {useHistory, useLocation} from "react-router-dom";
import {LANGUAGES_LABEL} from "../../constants";
import {pathnameToLanguage} from "../../utils/routing";

const MenuSettingsLanguage = ({onClose, selectedValue, open}) => {
    const intl = useIntl();

    const location = useLocation();
    const {canonical} = pathnameToLanguage(location.pathname);

    const history = useHistory();

    const handleLanguageClose = () => {
        onClose(selectedValue);
    };

    const handleLanguageListItemClick = (event) => {
        history.push(`/${event.target.value}${canonical}`);
        onClose(event.target.value);
    };

    return (
        <Dialog onClose={handleLanguageClose} aria-labelledby="menu-settings-language" open={open} fullWidth>
            <DialogTitle id="menu-settings-language">{intl.formatMessage({id: "settings.language"})}</DialogTitle>
            <DialogContent>
                <RadioGroup aria-label={intl.formatMessage({id: "settings.language"})} name="language"
                            value={selectedValue}
                            onChange={handleLanguageListItemClick}>
                    {LANGUAGES_LABEL.map((language) => (
                        <FormControlLabel key={language.code} value={language.code} control={<Radio />}
                                          label={language.text} />
                    ))}
                </RadioGroup>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleLanguageClose} color="primary">
                    {intl.formatMessage({id: "label.cancel"})}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

MenuSettingsLanguage.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    selectedValue: PropTypes.string.isRequired
};

export default MenuSettingsLanguage;