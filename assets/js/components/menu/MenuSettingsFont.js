import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import PropTypes from "prop-types";
import React from "react";
import {useIntl} from "react-intl";

const MenuSettingsFont = ({onClose, selectedValue, open}) => {
    const intl = useIntl();

    const handleFontClose = () => {
        onClose(selectedValue);
    };

    const handleFontListItemClick = (event) => {
        onClose(event.target.value);
    };

    return (
        <Dialog onClose={handleFontClose} aria-labelledby="menu-settings-font" open={open} fullWidth>
            <DialogTitle id="menu-settings-font">{intl.formatMessage({id: "settings.font"})}</DialogTitle>
            <DialogContent>
                <RadioGroup aria-label={intl.formatMessage({id: "settings.font"})} name="font" value={selectedValue}
                            onChange={handleFontListItemClick}>
                    <FormControlLabel key="default" value="default" control={<Radio />}
                                      label={intl.formatMessage({id: "settings.font.default"})} />
                    <FormControlLabel key="luciole" value="luciole" control={<Radio />}
                                      label={intl.formatMessage({id: "settings.font.luciole"})} />
                    <FormControlLabel key="opendys" value="opendys" control={<Radio />}
                                      label={intl.formatMessage({id: "settings.font.opendys"})} />
                </RadioGroup>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleFontClose} color="primary">
                    {intl.formatMessage({id: "label.cancel"})}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

MenuSettingsFont.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    selectedValue: PropTypes.string.isRequired
};

export default MenuSettingsFont;