import ImageListItemBar from "@material-ui/core/ImageListItemBar";
import React from "react";
import {Link} from "react-router-dom";
import {generateUrl} from "../../utils/routing";

const RoutineCard = ({locale, routine}) => (
    <Link to={generateUrl("/routines/" + routine.nameId, locale)}>
        <img
            src={"/img/routine/" + routine.nameId + ".jpg"}
            alt=""
            style={{width: "100%"}} />
        <ImageListItemBar
            title={routine.translations[locale].name}
            subtitle={<span>{routine.translations[locale].subname}</span>}
            /*actionIcon={
                <IconButton aria-label={`info about ${tile.title}`} className={classes.icon}>
                    <InfoIcon />
                </IconButton>
            }*/
        />
    </Link>
);

export default RoutineCard;
