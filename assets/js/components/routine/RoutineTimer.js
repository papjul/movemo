import PropTypes from "prop-types";
import React from "react";
import Timer from "react-compound-timer";
import RoutineStepper from "./RoutineStepper";

const RoutineTimer = ({locale, routineItems}) => (
    <Timer direction="backward" formatValue={(value) => `${(value < 10 ? `0${value}` : value)}`}>
        {({setCheckpoints, setTime, start, stop}) => (
            <RoutineStepper{...{locale, routineItems, setTime}} setTimerCheckpoints={setCheckpoints}
                           startTimer={start} stopTimer={stop} />
        )}
    </Timer>
);

RoutineTimer.propTypes = {
    locale: PropTypes.string.isRequired,
    //routineItems: PropTypes.arrayOf(RoutineItemsPropTypes).isRequired,
};

export default RoutineTimer;