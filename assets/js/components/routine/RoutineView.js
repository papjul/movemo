import Button from "@material-ui/core/Button";
import Fab from "@material-ui/core/Fab";
import MaterialUiLink from "@material-ui/core/Link";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import StartIcon from "@material-ui/icons/PlayArrow";
import React from "react";
import {useIntl} from "react-intl";
import Markdown from "react-markdown";
import {Link} from "react-router-dom";
import {generateUrl} from "../../utils/routing";

const useStyles = makeStyles((theme) => ({
    fab: {
        position: "fixed",
        bottom: `calc(56px + ${theme.spacing(2)})`,
        right: theme.spacing(2),
    },
}));

const RoutineView = ({locale, routine}) => {
    const intl = useIntl();
    const classes = useStyles();

    return (
        <>
            {/* Already shown on title bar */}
            <Typography variant="h3" component="h1" sx={{ display: { lg: 'block', xs: 'none' } }}>
                {routine.translations[locale].name}
            </Typography>

            <Typography variant="h5" component="h2">
                {routine.translations[locale].subname}
            </Typography>

            <Markdown source={routine.translations[locale].description} />

            <p>
                {routine.author && <>
                    <strong>{intl.formatMessage({id: "routine.author"})}{locale === "fr" && String.fromCharCode(8239)}:</strong> {routine.author}<br />
                </>}

                {routine.translations[locale].license && <>
                    <strong>{intl.formatMessage({id: "routine.license"})}{locale === "fr" && String.fromCharCode(8239)}:</strong> {routine.translations[locale].licenseUrl ? (
                    <MaterialUiLink href={routine.translations[locale].licenseUrl}
                                    rel="nofollow noreferrer">{routine.translations[locale].license}</MaterialUiLink>) : routine.translations[locale].license}<br />
                </>}

                {routine.link && <>
                    <strong>{intl.formatMessage({id: "routine.source"})}{locale === "fr" && String.fromCharCode(8239)}:</strong>{" "}
                    <MaterialUiLink
                        href={routine.link}
                        rel="nofollow noreferrer">{routine.link}</MaterialUiLink><br />
                </>}
            </p>

            <Button variant="contained" color="primary" component={Link}
                    to={generateUrl("/routines/" + routine.nameId + "/run", locale)}>
                {intl.formatMessage({id: "routine.start"})}
            </Button>

            <Fab color="primary" aria-label={intl.formatMessage({id: "routine.start"})} className={classes.fab}
                 component={Link} to={generateUrl("/routines/" + routine.nameId + "/run", locale)}
                 sx={{ display: { lg: 'none', xs: 'block' } }}>
                <StartIcon />
            </Fab>
        </>
    );
};

export default RoutineView;
