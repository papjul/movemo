import React, {useEffect, useRef} from "react";

const RoutineItemMedia = (({isPaused, isPreview, isRest, item, visible}) => {
    const vidRef = useRef(null);

    useEffect(() => {
        if (visible && item.useVideo) {
            vidRef.current.currentTime = 0;
        }
    }, [visible]);

    useEffect(() => {
        if (vidRef && vidRef.current) {
            if (isPaused) {
                vidRef.current.pause();
            } else {
                vidRef.current.play();
            }
        }
    }, [isPaused]);

    const video = "/video/exercise/" + item.nameId + (item.useAlternativeIllustration ? "_alt" : "") + ".mp4";
    const img = "/img/exercise/" + item.nameId + (item.useAlternativeIllustration ? "_alt" : "") + ".jpg";

    const className = (isPaused || isRest || isPreview) ? "preview" : "";

    const style = (visible) ? {} : {width: 0, height: 0}

    if (!item.useVideo) {
        return <img id="item_img" className={className} src={img} alt="" style={style} />
    } else {
        return (
            <video id="item_vid" className={className} autoPlay={true} loop={true} muted={true} ref={vidRef} style={style}>
                <source src={video} />
                <img src={img} alt="" />
            </video>
        );
    }
});

export default RoutineItemMedia;