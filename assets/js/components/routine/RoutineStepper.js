import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Fab from "@material-ui/core/Fab";
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import {KeyboardArrowLeft, KeyboardArrowRight} from "@material-ui/icons";
import PauseIcon from "@material-ui/icons/Pause";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import PropTypes from "prop-types";
import React, {useEffect, useState} from "react";
import Timer from "react-compound-timer";
import {useIntl} from "react-intl";
import RoutineItemMedia from "../../components/routine/RoutineItemMedia";
import RoutineProgress from "./RoutineProgress";

const useStyles = makeStyles((theme) => ({
    container: {
        minHeight: "calc(100vh - 56px)",
        height: "calc(100vh - 56px)",
        [`${theme.breakpoints.up("xs")} and (orientation: landscape)`]: {
            minHeight: "calc(100vh - 48px)",
            height: "calc(100vh - 48px)",
        },
        [theme.breakpoints.up("sm")]: {
            minHeight: "calc(100vh - 64px)",
            height: "calc(100vh - 64px)",
        },
    },
    title: {
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
    subtitle: {
        paddingTop: theme.spacing(1),
        marginBottom: theme.spacing(1)
    },
    quantity: {
        textAlign: "center",
        fontWeight: "bold",
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3)
    },
    cardTop: {
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(2),
        paddingLeft: theme.spacing(3),
        paddingRight: theme.spacing(3),
        marginBottom: "auto !important",
        width: "100%"
    },
    cardBody: {
        marginTop: "auto",
        width: "100%",
    },
    previewNextExercise: {
        textAlign: "center",
    },
    mobileStepper: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        background: theme.palette.background.default,
        padding: 8,
    },
}));

const RoutineStepper = ({locale, routineItems, setTime, setTimerCheckpoints, startTimer, stopTimer}) => {
    const intl = useIntl();
    const classes = useStyles();

    const [activeItem, setActiveItem] = useState(0);
    const maxItems = routineItems.length;

    const [isPaused, setPaused] = useState(false);
    const [isPreview, setPreview] = useState(false);

    const currentItem = (Array.isArray(routineItems[activeItem])) ? routineItems[activeItem][0] : routineItems[activeItem];

    const currentTitle = (currentItem.maxSet > 1)
        ? currentItem.name + " — Set " + currentItem.currentSet + "/" + currentItem.maxSet
        : currentItem.name;

    const currentVariant = () => {
        if (currentItem.variant || currentItem.note) {
            let variant = "";
            if (currentItem.variant) {
                variant += currentItem.variant;
                if (currentItem.note) {
                    variant += " | ";
                }
            }
            if (currentItem.note) {
                variant += currentItem.note;
            }
            return variant;
        } else {
            return null;
        }
    };

    const currentQuantity = () => {
        if (currentItem.isDuration) {
            return (<>
                <Timer.Minutes />:<Timer.Seconds />
            </>);
        } else {
            if (currentItem.quantity > 0) {
                if (currentItem.minQuantity > 0) {
                    return currentItem.minQuantity + "-" + currentItem.quantity + "×";
                } else {
                    return currentItem.quantity + "×";
                }
            } else {
                return "MAX";
            }
        }
    };

    const isRest = !("id" in currentItem) || currentItem.id === null || currentItem.id === 0;

    const isItemRest = (index) => !("id" in getItem(index)) || getItem(index).id === null || getItem(index).id === 0;

    const hasPrevItem = (activeItem !== 0);

    const hasNextItem = (activeItem !== maxItems - 1);

    const getItem = (item) => (Array.isArray(routineItems[item])) ? routineItems[item][0] : routineItems[item];

    // TODO: Replace with all in arrays
    const nextItem = hasNextItem ? ((Array.isArray(routineItems[activeItem + 1])) ? routineItems[activeItem + 1][0] : routineItems[activeItem + 1]) : null;
    const nextTitle = hasNextItem ? ((nextItem.maxSet > 1)
            ? nextItem.name + " — Set " + nextItem.currentSet + "/" + nextItem.maxSet
            : nextItem.name
    ) : null;

    const handleBack = () => {
        if (hasPrevItem) {
            stopTimer();
            setActiveItem((prevActiveItem) => prevActiveItem - 1);
        }
    };

    const handleItemChange = (item) => {
        if (item >= 0 && activeItem < maxItems) {
            stopTimer();
            setActiveItem(item);
        }
    };

    const handleNext = () => {
        if (hasNextItem) {
            stopTimer();
            setActiveItem((prevActiveItem) => prevActiveItem + 1);
        }
    };

    const previewNextItem = () => {
        if (hasNextItem) {
            stopTimer();
            setActiveItem((prevActiveItem) => prevActiveItem + 1);
            setPreview(true);
        }
    };

    const playSound = () => {
        if (audio) {
            audio.play();
        }
    };

    const playMidSound = () => {
        if (audioMid) {
            audioMid.play();
        }
    };

    const initTimer = () => {
        if (currentItem.isDuration) {
            setTime(currentItem.quantity * 1000);

            let timerCheckpoints = [];
            timerCheckpoints.push({
                time: 0,
                callback: () => timeIsUp(),
            });

            if (currentItem.minQuantity > 0) {
                timerCheckpoints.push({
                    time: (currentItem.quantity - currentItem.minQuantity) * 1000,
                    callback: () => timeIsMid(),
                });
            }
            setTimerCheckpoints(timerCheckpoints);

            if (!isPaused) {
                startTimer();
            }
        } else {
            setTime(null);
            setTimerCheckpoints([]);
        }
    };

    const handlePause = () => {
        if (isPaused) {
            startTimer();
            setPaused(false);
        } else {
            stopTimer();
            setPaused(true);
        }
    };

    const onKeydown = (e) => {
        if (e.key === "ArrowRight" || e.key === "ArrowDown") {
            handleNext();
        } else if (e.key === "ArrowLeft" || e.key === "ArrowUp") {
            handleBack();
        } else if (e.key === " " || e.key === "Spacebar") {
            handlePause();
        }
    };

    // First render
    const [audio, setAudio] = useState(null);
    const [audioMid, setAudioMid] = useState(null);
    useEffect(() => {
        // Client side only
        setAudio(new Audio("/timer.mp3"));
        setAudioMid(new Audio("/timer_milestone.mp3"));
        if (currentItem.isDuration) {
            initTimer();
        }
        document.addEventListener("keydown", onKeydown);
        return () => { // Remove event listeners on cleanup
            document.removeEventListener("keydown", onKeydown);
        };
    }, []); // Empty array ensures that effect is only run on mount and unmount

    // On item change
    useEffect(() => {
        if (currentItem.isDuration) {
            initTimer();
        }
    }, [activeItem]);

    // On preview change
    useEffect(() => {
        if (!isPreview) {
            initTimer();
        } else {
            setTime(5 * 1000);
            setTimerCheckpoints([
                {
                    time: 0,
                    callback: () => timeIsUp(),
                }
            ]);

            if (!isPaused) {
                startTimer();
            }
        }
    }, [isPreview]);

    // On item change or is paused
    useEffect(() => {
        document.addEventListener("keydown", onKeydown);
        return () => { // Remove event listeners on cleanup
            document.removeEventListener("keydown", onKeydown);
        };
    }, [activeItem, isPaused]);

    const timeIsUp = () => {
        playSound();
        if (isPreview) {
            setPreview(false); // Callback
        } else if (nextItem.id === 0 || nextItem.id === null || currentItem.id === 0 || currentItem.id === null) {
            handleNext();
        } else if (nextItem.isDuration) {
            previewNextItem();
        } else {
            handleNext();
        }
    };

    const timeIsMid = () => {
        playMidSound();
    };

    return (
        <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column"}}
             className={classes.container} component={Paper}>
            <Box className={classes.cardTop}>
                <Typography variant="h4" component="h1"
                            className={classes.title} align="center">{currentTitle}</Typography>

                {!isPaused && (currentItem.variant || currentItem.note) &&
                <Typography variant="subtitle1"
                            className={classes.subtitle}
                            align="center">{currentVariant()}</Typography>}

                {isRest && hasNextItem &&
                <p className={classes.previewNextExercise}>
                    {intl.formatMessage({id: "routine.next_exercise"})}{locale === "fr" && " "}: {nextTitle}
                </p>}
            </Box>

            {routineItems.map((item, index) => (
                Math.abs(activeItem - index) <= 1
                    ? (
                        <RoutineItemMedia key={index} item={isItemRest(index) ? getItem(index + 1) : getItem(index)}
                                          isPaused={isPaused} isPreview={isPreview} isRest={isItemRest(index)}
                                          visible={activeItem === index} />
                    ) : null
            ))}

            <Box className={classes.cardBody}>
                {<Typography variant="h4" component="p"
                             className={classes.quantity}>{currentQuantity()}</Typography>}

                <RoutineProgress activeItem={activeItem} maxItems={maxItems} />
                <Paper square elevation={0} className={classes.mobileStepper}>
                    <Button size="large" onClick={handleBack} disabled={!hasPrevItem}>
                        <KeyboardArrowLeft />
                        {intl.formatMessage({id: "routine.previous"})}
                    </Button>

                    <Tooltip
                        title={intl.formatMessage({id: isPaused ? "routine.resume" : "routine.pause"})}
                        aria-label="play/pause">
                        <Fab color="secondary" aria-label="play/pause" onClick={handlePause} size="small">
                            {isPaused ? <PlayArrowIcon /> : <PauseIcon />}
                            {/* TODO: On pause, fetch details of exercise */}
                        </Fab>
                    </Tooltip>

                    <Button size="large" onClick={handleNext} disabled={!hasNextItem}>
                        {intl.formatMessage({id: "routine.next"})}
                        <KeyboardArrowRight />
                    </Button>
                </Paper>
            </Box>
        </Box>
    );
};

RoutineStepper.propTypes = {
    locale: PropTypes.string.isRequired,
    //routineItems: PropTypes.arrayOf(RoutineItemsPropTypes).isRequired,
};

export default RoutineStepper;