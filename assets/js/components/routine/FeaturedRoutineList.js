import Button from "@material-ui/core/Button";
import ImageList from "@material-ui/core/ImageList";
import ImageListItem from "@material-ui/core/ImageListItem";
import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";
import {useIntl} from "react-intl";
import {Link} from "react-router-dom";
import {generateUrl} from "../../utils/routing";
import RoutineCard from "./RoutineCard";

const useStyles = makeStyles(theme => ({
    featuredRoutinesList: {
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(2),
        // Override number of cols through CSS for Server-Side Rendering support
        [theme.breakpoints.up("lg")]: {
            gridTemplateColumns: "repeat(4, 1fr) !important",
        },
        [theme.breakpoints.only("md")]: {
            gridTemplateColumns: "repeat(3, 1fr) !important",
        },
        [theme.breakpoints.only("sm")]: {
            gridTemplateColumns: "repeat(2, 1fr) !important",
        },
        [theme.breakpoints.only("xs")]: {
            gridTemplateColumns: "repeat(1, 1fr) !important",
        },
    },
}));

const FeaturedRoutineList = ({featuredRoutines, locale}) => {
    const intl = useIntl();
    const classes = useStyles();

    return (
        <>
            <Typography variant="h4" component="h1">
                {intl.formatMessage({id: "home.featured_routines"})}
            </Typography>

            <ImageList rowHeight={160} className={classes.featuredRoutinesList}>
                {Object.values(featuredRoutines).map((routine) => (
                    <ImageListItem key={routine.nameId} cols={1}>
                        <RoutineCard {...{routine, locale}} />
                    </ImageListItem>
                ))}
            </ImageList>

            <Button variant="contained" color="primary" aria-label={intl.formatMessage({id: "home.see_all"})}
                    component={Link} to={generateUrl("/routines", locale)}>
                {intl.formatMessage({id: "home.see_all"})}
            </Button>
        </>
    );
};

export default FeaturedRoutineList;