import ImageList from "@material-ui/core/ImageList";
import ImageListItem from "@material-ui/core/ImageListItem";
import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";
import RoutineCard from "./RoutineCard";

const useStyles = makeStyles(theme => ({
    routinesList: {
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(2),
        // Override number of cols through CSS for Server-Side Rendering support
        [theme.breakpoints.up("lg")]: {
            gridTemplateColumns: "repeat(4, 1fr) !important",
        },
        [theme.breakpoints.only("md")]: {
            gridTemplateColumns: "repeat(3, 1fr) !important",
        },
        [theme.breakpoints.only("sm")]: {
            gridTemplateColumns: "repeat(2, 1fr) !important",
        },
        [theme.breakpoints.only("xs")]: {
            gridTemplateColumns: "repeat(1, 1fr) !important",
        },
    },
}));

const RoutineCategoryList = ({filterText, locale, routineCategories, routines}) => {
    const classes = useStyles();

    const filterRoutines = categoryId => {
        return routineCategories[categoryId].routines.filter(routineId => {
            if (filterText !== "" &&
                (!routines[routineId].translations[locale].name ||
                    routines[routineId].translations[locale].name.toLowerCase().indexOf(filterText) === -1) &&
                (!routines[routineId].translations[locale].subname ||
                    routines[routineId].translations[locale].subname.toLowerCase().indexOf(filterText) === -1) &&
                (!routines[routineId].translations[locale].description ||
                    routines[routineId].translations[locale].description.toLowerCase().indexOf(filterText) === -1)
            ) {
                return false;
            }
            return true;
        });
    };

    return (
        <>
            {Object.values(routineCategories).map((routineCategory) => (
                <div key={routineCategory.id}>
                    {filterRoutines(routineCategory.id).length > 0 && <>
                        <Typography component="h2" variant="h4">
                            {routineCategory.translations[locale].name}
                        </Typography>
                        <ImageList rowHeight={160} className={classes.routinesList}>
                            {filterRoutines(routineCategory.id).map(routineId => (
                                <ImageListItem key={routineId} cols={1}>
                                    <RoutineCard routine={routines[routineId]} locale={locale} />
                                </ImageListItem>
                            ))}
                        </ImageList>
                    </>}
                </div>
            ))}
        </>
    );
};

export default RoutineCategoryList;