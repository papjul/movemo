import LinearProgress from "@material-ui/core/LinearProgress";
import PropTypes from "prop-types";
import React from "react";

const RoutineProgress = ({activeItem, maxItems}) => (
    <LinearProgress variant="determinate" value={(activeItem / (maxItems - 1)) * 100} />
);

RoutineProgress.propTypes = {
    activeItem: PropTypes.number.isRequired,
    maxItems: PropTypes.number.isRequired
};

export default RoutineProgress;