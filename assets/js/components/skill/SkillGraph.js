import Cytoscape from "cytoscape";
import Dagre from "cytoscape-dagre";
import React from "react";
import CytoscapeComponent from "react-cytoscapejs/src/component";

const SkillGraph = ({exercises, progressions}) => {
    const styles = [
        {
            selector: "node",
            style: {
                "height": 30,
                "width": 30,
                //"text-valign": "center",
                //"text-halign": "center",
                "color": "#00bc8c",
                "background-fit": "cover",
                "label": "data(name)"
            }
        },
        {
            selector: "edge",
            style: {
                "curve-style": "bezier",
                "width": 2,
                "target-arrow-shape": "triangle",
                "line-color": "white",
                "target-arrow-color": "white"
            }
        },
        {
            selector: ".milestone",
            style: {
                "height": 80,
                "width": 80,
                //'border-color': '#00bc8c',
                //'border-opacity': 0.5,
                "border-width": 4,
            }
        },
        {
            selector: ".beginner",
            style: {
                "border-color": "#00bc8c",
                "background-color": "#00bc8c",
                "color": "#00bc8c",
            }
        },
        {
            selector: ".intermediate",
            style: {
                "border-color": "#40c4ff",
                "background-color": "#40c4ff",
                "color": "#40c4ff"
            }
        },
        {
            selector: ".advanced",
            style: {
                "border-color": "#ffd740",
                "background-color": "#ffd740",
                "color": "#ffd740"
            }
        },
        {
            selector: ".elite",
            style: {
                "border-color": "#ff5252",
                "background-color": "#ff5252",
                "color": "#ff5252"
            }
        }
    ];

    let nodes = [];
    Object.values(exercises).map(exercise => {
        let itemClasses = [exercise.level];
        if (exercise.milestone) {
            itemClasses.push("milestone");
        }

        let item = {
            data: {
                id: exercise.nameId,
                name: exercise.name
            },
            image: "/img/exercise/" + exercise.nameId + ".jpg",
            classes: itemClasses,
            grabbable: true, // whether the node can be grabbed and moved by the user
        };

        nodes.push(item);

        if (exercise.milestone) {
            styles.push({
                selector: "#" + exercise.nameId,
                style: {
                    "background-image": "/img/exercise/" + exercise.nameId + ".jpg"
                }
            });
        }
    });

    let edges = [];
    Object.values(progressions).map(progression => {
        edges.push({
            data: {
                source: progression.source.nameId,
                target: progression.target.nameId
            }
        });
    });

    const layout = {
        name: "dagre",
        //nodeDimensionsIncludeLabels: true,
        rankDir: "LR",
        ranker: "network-simplex", // 'network-simplex', 'tight-tree' or 'longest-path'
        //directed: true,
        //padding: 10,
        //grid: true,
        //avoidOverlap: true
    };
    Cytoscape.use(Dagre);

    return (
        <CytoscapeComponent userPanningEnabled={false} boxSelectionEnabled={false} layout={layout} stylesheet={styles}
                            elements={CytoscapeComponent.normalizeElements({nodes: nodes, edges: edges})}
                            style={{width: "100%", minWidth: 1900, minHeight: 2500}} />
    );
};

export default SkillGraph;
