import {Paper} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import React, {useState} from "react";
import {useIntl} from "react-intl";
import {useHistory} from "react-router-dom";
import {generateUrl} from "../../utils/routing";

const useStyles = makeStyles(theme => ({
    hideOnMobile: {
        [theme.breakpoints.down("sm")]: {
            display: "none"
        }
    },
    tableRow: {
        height: `calc(50px + ${theme.spacing(2)})`,
        cursor: "pointer"
    }
}));

const ExerciseList = ({exercises, filterText, locale}) => {
    const intl = useIntl();
    const classes = useStyles();
    const history = useHistory();

    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);

    const filterExercises = () => {
        return Object.values(exercises).filter(exercise => {
            console.log(exercise);
            if (filterText !== "" &&
                (!exercise.translations[locale].name ||
                    exercise.translations[locale].name.toLowerCase().indexOf(filterText) === -1) &&
                (!exercise.translations["en"].name ||
                    exercise.translations["en"].name.toLowerCase().indexOf(filterText) === -1) &&
                (!exercise.translations[locale].description ||
                    exercise.translations[locale].description.toLowerCase().indexOf(filterText) === -1)
            ) {
                return false;
            }
            return true;
        });
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const goToExercise = (name_id) => {
        history.push(generateUrl("/exercises/" + name_id, locale));
    };

    return (
        <>
            <TableContainer component={Paper} className={classes.table}>
                <Table aria-label="exercise table">
                    <TableHead>
                        <TableRow>
                            <TableCell>#</TableCell>
                            <TableCell>{intl.formatMessage({id: "exercise.name"})}</TableCell>
                            {locale !== "en" && <TableCell
                                className={classes.hideOnMobile}>{intl.formatMessage({id: "exercise.name_in_english"})}</TableCell>}
                            <TableCell>{intl.formatMessage({id: "exercise.source"})}</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {filterExercises().slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(exercise => (
                            <TableRow key={exercise.nameId} hover
                                      onClick={() => goToExercise(exercise.nameId)}
                                      className={classes.tableRow}>{/* TODO: Add support for link with JavaScript disabled */}
                                <TableCell component="th" scope="row">
                                    <img src={"/img/exercise/" + exercise.nameId + ".jpg"} alt=""
                                         style={{
                                             maxWidth: "50px",
                                             maxHeight: "50px",
                                         }} />
                                </TableCell>
                                <TableCell>{exercise.translations[locale].name}</TableCell>
                                {locale !== "en" &&
                                <TableCell className={classes.hideOnMobile}>
                                    {exercise.translations["en"].name}
                                </TableCell>
                                }
                                <TableCell>{exercise.source}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[5, 10, 25, 50, 100]}
                component="div"
                count={exercises.size}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </>
    );
};

export default ExerciseList;