import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import MaterialUiLink from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import React from "react";
import {useIntl} from "react-intl";
import Markdown from "react-markdown";

const ExerciseView = ({exercise, locale}) => {
    const intl = useIntl();

    return (
        <Card>
            <CardHeader title={exercise.translations[locale].name} sx={{ display: { lg: 'block', xs: 'none' } }} />
            <CardMedia image={"/img/exercise/" + exercise.nameId + ".jpg"}>
                {exercise.hasVideo &&
                <video width="100%" autoPlay loop>
                    <source src={"/video/exercise/" + exercise.nameId + ".mp4"} />
                    <img style={{width: "100%", display: "block"}}
                         src={"/img/exercise/" + exercise.nameId + ".jpg"} alt="" />
                </video>
                }
            </CardMedia>
            <CardContent>
                {(exercise.source || exercise.sourceUrl) &&
                <>
                    <strong>{intl.formatMessage({id: "exercise.source"})}{locale === "fr" && String.fromCharCode(8239)}:</strong> {exercise.sourceUrl ? (
                    <MaterialUiLink href={exercise.sourceUrl} rel="nofollow noreferrer">
                        {exercise.source ? exercise.source : exercise.sourceUrl}
                    </MaterialUiLink>
                ) : exercise.source}<br />
                </>
                }

                {exercise.translations[locale].description &&
                <>
                    <Typography variant="h5" component="h2">
                        {intl.formatMessage({id: "exercise.description"})}
                    </Typography>
                    <Markdown source={exercise.translations[locale].description} />
                </>
                }

                {exercise.translations[locale].evolution &&
                <>
                    <Typography variant="h5" component="h2">
                        {intl.formatMessage({id: "exercise.evolution"})}
                    </Typography>
                    <Markdown source={exercise.translations[locale].evolution} />
                </>
                }
            </CardContent>
        </Card>
    );
};

export default ExerciseView;
