import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import React, {useState} from "react";
import Timer from "react-compound-timer";
import {useIntl} from "react-intl";
import Bubble from "./Bubble";

const useStyles = makeStyles(theme => ({
    relaxationContainer: {
        minHeight: "calc(100vh - 56px - 56px - " + theme.spacing(6) + ")",
        height: "calc(100vh - 56px - 56px - " + theme.spacing(6) + ")",
        [`${theme.breakpoints.up("xs")} and (orientation: landscape)`]: {
            minHeight: "calc(100vh - 48px - 56px - " + theme.spacing(6) + ")",
            height: "calc(100vh - 48px - 56px - " + theme.spacing(6) + ")",
        },
        [theme.breakpoints.up("sm")]: {
            minHeight: "calc(100vh - 64px - 56px - " + theme.spacing(6) + ") !important",
            height: "calc(100vh - 64px - 56px - " + theme.spacing(6) + ") !important",
        },
        [theme.breakpoints.up("lg")]: {
            minHeight: "calc(100vh - 56px - 82px - " + theme.spacing(6) + ")",
            height: "calc(100vh - 56px - 82px - " + theme.spacing(6) + ")",
        },
    },

    circleContainer: {
        flexShrink: 1,
        minHeight: 0,
        height: "auto",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        alignContent: "center",
        "& > div:not(.fields)": {
            background: theme.palette.secondary.light,
            borderRadius: "50%",
            width: "150px",
            height: "150px",
            [theme.breakpoints.down("xs")]: {
                width: "100px",
                height: "100px",
            },
        }
    },

    fieldsContainer: {
        marginTop: theme.spacing(3),
    },

    timer: {
        marginTop: theme.spacing(8),
        textAlign: "center"
    }
}));

const Breathe = () => {
    const intl = useIntl();
    const classes = useStyles();
    const [inhale, setInhale] = useState(5);
    const [holdAfterInhale, setHoldAfterInhale] = useState(0);
    const [exhale, setExhale] = useState(5);
    const [holdAfterExhale, setHoldAfterExhale] = useState(0);
    const [duration, setDuration] = useState(5); // Infinity
    const [count, setCount] = useState(0);
    const [open, setOpen] = useState(false);
    const [preselection, setPreselection] = useState("0"); // Infinity
    const preselections = [
        {
            label: intl.formatMessage({id: "breathe.option.cardiac_coherence"}),
            inhale: 5,
            holdAfterInhale: 0,
            exhale: 5,
            holdAfterExhale: 0,
            duration: 5
        }
    ];

    return (
        <Timer initialTime={duration * 60 * 1000} direction="backward"
               formatValue={(value) => `${(value < 10 ? `0${value}` : value)}`}>
            {({reset, setTime}) => {
                function refresh(customDuration) {
                    setCount(count + 1);
                    setTime((customDuration ? customDuration : duration) * 60 * 1000);
                    reset();
                }

                const handleClickOpen = () => {
                    setOpen(true);
                };

                const handleClose = () => {
                    setOpen(false);
                };

                const handleSave = () => {
                    setPreselection("custom");
                    refresh();
                    setOpen(false);
                };

                const handlePreselectChange = (e) => {
                    setPreselection(e.target.value);
                    if (e.target.value !== "custom") {
                        setInhale(preselections[e.target.value].inhale);
                        setHoldAfterInhale(preselections[e.target.value].holdAfterInhale);
                        setExhale(preselections[e.target.value].exhale);
                        setHoldAfterExhale(preselections[e.target.value].holdAfterExhale);
                        setDuration(preselections[e.target.value].duration);
                        refresh(preselections[e.target.value].duration);
                    } else {
                        handleClickOpen();
                    }
                };

                return (
                    <Grid container direction="column" className={classes.relaxationContainer} spacing={2}>
                        <Grid item style={{marginBottom: "auto"}}>
                            <Grid container justifyContent="center" spacing={2}>
                                <Grid item>
                                    <TextField
                                        id="preselection"
                                        select
                                        label={intl.formatMessage({id: "breathe.preselection"})}
                                        value={preselection}
                                        onChange={handlePreselectChange}
                                        SelectProps={{
                                            native: true,
                                        }}
                                        variant="outlined"
                                    >
                                        {preselections.map((option, idx) => (
                                            <option key={idx} value={idx}>
                                                {option.label} ({option.inhale} / {option.holdAfterInhale} / {option.exhale} / {option.holdAfterExhale})
                                            </option>
                                        ))}
                                        <option key="custom" value="custom">
                                            {intl.formatMessage({id: "breathe.option.customized"})}
                                        </option>
                                    </TextField>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                            <DialogTitle id="form-dialog-title">
                                {intl.formatMessage({id: "breathe.customize"})}
                            </DialogTitle>
                            <DialogContent>
                                <Grid container spacing={2}>
                                    {/* TODO: Add preselections */}
                                    <Grid item>
                                        <TextField
                                            label={intl.formatMessage({id: "breathe.inhale"})}
                                            type="number"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            onChange={(e) => setInhale(parseInt(e.target.value))}
                                            variant="outlined"
                                            value={inhale}
                                        />
                                    </Grid>
                                    <Grid item>
                                        <TextField
                                            label={intl.formatMessage({id: "breathe.hold_after_inhale"})}
                                            type="number"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            onChange={(e) => setHoldAfterInhale(parseInt(e.target.value))}
                                            variant="outlined"
                                            value={holdAfterInhale}
                                        />
                                    </Grid>
                                    <Grid item>
                                        <TextField
                                            label={intl.formatMessage({id: "breathe.exhale"})}
                                            type="number"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            onChange={(e) => setExhale(parseInt(e.target.value))}
                                            variant="outlined"
                                            value={exhale}
                                        />
                                    </Grid>
                                    <Grid item>
                                        <TextField
                                            label={intl.formatMessage({id: "breathe.hold_after_exhale"})}
                                            type="number"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            onChange={(e) => setHoldAfterExhale(parseInt(e.target.value))}
                                            variant="outlined"
                                            value={holdAfterExhale}
                                        />
                                    </Grid>
                                    <Grid item>
                                        <TextField
                                            label={intl.formatMessage({id: "breathe.duration"})}
                                            type="number"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            onChange={(e) => setDuration(parseInt(e.target.value))}
                                            variant="outlined"
                                            value={duration}
                                        />
                                    </Grid>
                                </Grid>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleClose} color="primary">
                                    {intl.formatMessage({id: "label.cancel"})}
                                </Button>
                                <Button onClick={handleSave} color="primary">
                                    {intl.formatMessage({id: "label.save"})}
                                </Button>
                            </DialogActions>
                        </Dialog>
                        <Grid item className={classes.circleContainer}>
                            <Bubble key={count} {...{inhale, holdAfterInhale, exhale, holdAfterExhale, duration}} />
                        </Grid>
                        <Grid item className={classes.timer} style={{marginTop: "auto"}}>
                            <Typography color="primary" component="p" variant="h4">
                                <Timer.Minutes />:<Timer.Seconds />
                            </Typography>
                        </Grid>
                    </Grid>
                );
            }}
        </Timer>
    );
};

export default Breathe;
