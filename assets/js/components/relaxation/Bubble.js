import {motion} from "framer-motion";
import PropTypes from "prop-types";
import React from "react";

const Bubble = ({inhale, holdAfterInhale, exhale, holdAfterExhale, duration}) => {
    const total = inhale + holdAfterInhale + exhale + holdAfterExhale;
    const inhaleStep = (inhale / total);
    const holdAfterInhaleStep = ((inhale + holdAfterInhale) / total);
    const exhaleStep = ((inhale + holdAfterInhale + exhale) / total);
    const nbTimes = Math.ceil((duration * 60) / total);

    return (
        <motion.div
            animate={{
                scale: [1, 2, 2, 1, 1]
            }}
            transition={{
                times: [0, inhaleStep, holdAfterInhaleStep, exhaleStep, 1],
                loop: nbTimes,
                duration: total,
            }}
        />
    );
};

Bubble.propTypes = {
    inhale: PropTypes.number.isRequired,
    holdAfterInhale: PropTypes.number.isRequired,
    exhale: PropTypes.number.isRequired,
    holdAfterExhale: PropTypes.number.isRequired,
    duration: PropTypes.number.isRequired
};

export default Bubble;
