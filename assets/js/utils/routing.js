const {LANGUAGES} = require("../constants");

export function generateUrl(location, locale = null) {
    if (locale === null) {
        locale = "en";
    }
    return "/" + locale + location;
}

export function pathnameToLanguage(pathname) {
    if (pathname === "/" || pathname === "") {
        return {
            userLanguage: null,
            canonical: ""
        };
    }

    const userLanguage = pathname.substring(1, 3);

    if (LANGUAGES.indexOf(userLanguage) !== -1 && pathname.indexOf(`/${userLanguage}`) === 0) {
        return {
            userLanguage,
            canonical: pathname.substring(3),
        };
    }

    return {
        userLanguage: "en",
        canonical: pathname,
    };
}