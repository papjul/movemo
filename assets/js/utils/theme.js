import deepPurple from "@material-ui/core/colors/deepPurple";
import red from "@material-ui/core/colors/red";
import teal from "@material-ui/core/colors/teal";
import {enUS, frFR} from "@material-ui/core/locale";
import {createMuiTheme} from "@material-ui/core/styles";
import Luciole from "../../fonts/Luciole/Luciole-Regular.ttf";
import OpenDyslexicWoff from "../../fonts/OpenDyslexic/OpenDyslexic-Regular.woff";
import OpenDyslexicWoff2 from "../../fonts/OpenDyslexic/OpenDyslexic-Regular.woff2";

const opendys = {
    fontFamily: "OpenDyslexic",
    fontStyle: "normal",
    fontDisplay: "swap",
    fontWeight: 400,
    src: `
        local('OpenDyslexic'),
        local('Open Dyslexic'),
        local('OpenDyslexic-Regular'),
        local('Open Dyslexic-Regular'),
        url(${OpenDyslexicWoff2}) format('woff2'),
        url(${OpenDyslexicWoff}) format('woff')
      `,
    unicodeRange:
        "U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF",
};

const luciole = {
    fontFamily: "Luciole",
    fontStyle: "normal",
    fontDisplay: "swap",
    fontWeight: 400,
    src: `
        local('Luciole'),
        local('Luciole-Regular'),
        url(${Luciole}) format('truetype')
      `,
    unicodeRange:
        "U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF",
};

const getThemeSource = (mode = "light", locale = "en", font = "default") => {
    const localeObject = (locale === "fr") ? frFR : enUS;

    let theme = {
        //spacing: x => x * 8,
        palette: {
            primary: teal,
            secondary: deepPurple,
            error: red,
            mode: mode,
        },
    };

    if (font === "opendys") {
        theme.typography = {
            fontFamily: `"OpenDyslexic", "Roboto", "Helvetica", "Arial", sans-serif`,
            fontSize: 12,
        };
        theme.overrides = {
            MuiCssBaseline: {
                "@global": {
                    "@font-face": [opendys],
                },
            },
        };
    } else if (font === "luciole") {
        theme.typography = {
            fontFamily: `"Luciole", "Roboto", "Helvetica", "Arial", sans-serif`,
        };
        theme.overrides = {
            MuiCssBaseline: {
                "@global": {
                    "@font-face": [luciole],
                },
            },
        };
    }

    return createMuiTheme(theme, localeObject);
};

export default getThemeSource;