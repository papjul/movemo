const getLocaleMessages = (l, ls) => {
    if (ls) {
        for (let i = 0; i < ls.length; i++) {
            if (ls[i]["locale"] === l) {
                return ls[i]["messages"];
            }
        }
    }

    return {};
};

const formatMessage = (messages = [], id) => {
    return messages[id] || id;
};

export {
    formatMessage,
    getLocaleMessages
};