<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * RoutineSequence.
 *
 * @ORM\Entity
 */
class RoutineSequence
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Routine
     *
     * @ORM\ManyToOne(targetEntity="Routine", inversedBy="routineSequences")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(referencedColumnName="id")
     * })
     */
    private $routine;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=false)
     */
    private $sequenceOrder;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $minNbSets;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=false)
     */
    private $nbSets = 1;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=false)
     */
    private $secBetweenSets = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=false)
     */
    private $secAfterSequence = 0;

    /**
     * @var RoutineSequenceItem[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="RoutineSequenceItem",
     *      mappedBy="routineSequence",
     *      orphanRemoval=true,
     *      cascade={"persist"}
     * )
     * @ORM\OrderBy({"itemOrder": "ASC"})
     */
    private $sequenceItems;

    public function __construct()
    {
        $this->sequenceItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSequenceOrder(): ?int
    {
        return $this->sequenceOrder;
    }

    public function setSequenceOrder(int $sequenceOrder): self
    {
        $this->sequenceOrder = $sequenceOrder;

        return $this;
    }

    public function getRoutine(): ?Routine
    {
        return $this->routine;
    }

    public function setRoutine(?Routine $routine): self
    {
        $this->routine = $routine;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): RoutineSequence
    {
        $this->type = $type;

        return $this;
    }

    public function getNbSets(): ?int
    {
        return $this->nbSets;
    }

    public function setNbSets(int $nbSets): self
    {
        $this->nbSets = $nbSets;

        return $this;
    }

    public function getSecBetweenSets(): ?int
    {
        return $this->secBetweenSets;
    }

    public function setSecBetweenSets(int $secBetweenSets): self
    {
        $this->secBetweenSets = $secBetweenSets;

        return $this;
    }

    public function getSecAfterSequence(): ?int
    {
        return $this->secAfterSequence;
    }

    public function setSecAfterSequence(int $secAfterSequence): self
    {
        $this->secAfterSequence = $secAfterSequence;

        return $this;
    }

    /**
     * @return int
     */
    public function getMinNbSets(): ?int
    {
        return $this->minNbSets;
    }

    /**
     * @param int $minNbSets
     */
    public function setMinNbSets(?int $minNbSets): void
    {
        $this->minNbSets = $minNbSets;
    }

    public function getSequenceItems(): Collection
    {
        return $this->sequenceItems;
    }
}
