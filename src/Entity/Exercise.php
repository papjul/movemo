<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * Exercise.
 *
 * @ORM\Entity(repositoryClass="App\Repository\ExerciseRepository")
 */
class Exercise implements TranslatableInterface
{
    use TranslatableTrait;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=80, nullable=false)
     */
    private $name_id;

    /**
     * @var \Level
     *
     * @ORM\ManyToOne(targetEntity="Level")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(referencedColumnName="id")
     * })
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $source_url;

    /**
     * @var bool
     *
     * @ORM\Column(type="smallint", nullable=false)
     */
    private $hasVideo;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Equipment", inversedBy="exercises")
     * @ORM\JoinTable(
     *   joinColumns={
     *     @ORM\JoinColumn(referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(referencedColumnName="id")
     *   }
     * )
     */
    private $equipments;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Muscle", inversedBy="exercises")
     * @ORM\JoinTable(
     *   joinColumns={
     *     @ORM\JoinColumn(referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(referencedColumnName="id")
     *   }
     * )
     */
    private $muscles;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $milestone;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->equipments = new ArrayCollection();
        $this->muscles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLevel(): ?Level
    {
        return $this->level;
    }

    public function setLevel(?Level $level): self
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return Collection|Equipment[]
     */
    public function getEquipments(): Collection
    {
        return $this->equipments;
    }

    public function addEquipment(Equipment $equipment): self
    {
        if (!$this->equipments->contains($equipment)) {
            $this->equipments[] = $equipment;
        }

        return $this;
    }

    public function removeEquipments(Equipment $equipment): self
    {
        if ($this->equipments->contains($equipment)) {
            $this->equipments->removeElement($equipment);
        }

        return $this;
    }

    /**
     * @return Collection|Muscle[]
     */
    public function getMuscles(): Collection
    {
        return $this->muscles;
    }

    public function addMuscle(Muscle $muscle): self
    {
        if (!$this->muscles->contains($muscle)) {
            $this->muscles[] = $muscle;
        }

        return $this;
    }

    public function removeMuscle(Muscle $muscle): self
    {
        if ($this->muscles->contains($muscle)) {
            $this->muscles->removeElement($muscle);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function getHasVideo(): bool
    {
        return $this->hasVideo;
    }

    /**
     * @param bool $hasVideo
     */
    public function setHasVideo(bool $hasVideo): void
    {
        $this->hasVideo = $hasVideo;
    }

    /**
     * @return string
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(?string $source): void
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getSourceUrl(): ?string
    {
        return $this->source_url;
    }

    /**
     * @param string $source_url
     */
    public function setSourceUrl(?string $source_url): void
    {
        $this->source_url = $source_url;
    }

    /**
     * @return string
     */
    public function getNameId(): string
    {
        return $this->name_id;
    }

    /**
     * @param string $name_id
     */
    public function setNameId(string $name_id): void
    {
        $this->name_id = $name_id;
    }

    /**
     * @return bool
     */
    public function isMilestone(): bool
    {
        return $this->milestone;
    }

    /**
     * @param bool $milestone
     */
    public function setMilestone(bool $milestone): void
    {
        $this->milestone = $milestone;
    }

}
