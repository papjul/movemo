<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * RoutineSequenceItem.
 *
 * @ORM\Entity
 */
class RoutineSequenceItem implements TranslatableInterface
{
    use TranslatableTrait;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=false)
     */
    private $itemOrder = 1;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $minQuantity;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=false)
     */
    private $quantity;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $isDuration;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $useStaticImage;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $useAlternativeIllustration;

    /**
     * @var Exercise|null
     *
     * @ORM\ManyToOne(targetEntity="Exercise")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(referencedColumnName="id")
     * })
     */
    private $exercise;

    /**
     * @var RoutineSequence
     *
     * @ORM\ManyToOne(targetEntity="RoutineSequence", inversedBy="sequenceItems")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(referencedColumnName="id")
     * })
     */
    private $routineSequence;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getItemOrder(): ?int
    {
        return $this->itemOrder;
    }

    public function setItemOrder(int $itemOrder): self
    {
        $this->itemOrder = $itemOrder;

        return $this;
    }

    public function getExercise(): ?Exercise
    {
        return $this->exercise;
    }

    public function setExercise(?Exercise $exercise): self
    {
        $this->exercise = $exercise;

        return $this;
    }

    public function getRoutineSequence(): ?RoutineSequence
    {
        return $this->routineSequence;
    }

    public function setRoutineSequence(?RoutineSequence $routineSequence): self
    {
        $this->routineSequence = $routineSequence;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDuration(): bool
    {
        return $this->isDuration;
    }

    /**
     * @param bool $isDuration
     */
    public function setIsDuration(bool $isDuration): void
    {
        $this->isDuration = $isDuration;
    }

    /**
     * @return bool
     */
    public function isUseStaticImage(): ?bool
    {
        return $this->useStaticImage;
    }

    /**
     * @param bool $useStaticImage
     */
    public function setUseStaticImage(bool $useStaticImage): void
    {
        $this->useStaticImage = $useStaticImage;
    }

    /**
     * @return int
     */
    public function getMinQuantity(): ?int
    {
        return $this->minQuantity;
    }

    /**
     * @param int $minQuantity
     */
    public function setMinQuantity(?int $minQuantity): void
    {
        $this->minQuantity = $minQuantity;
    }

    /**
     * @return ?bool
     */
    public function isUseAlternativeIllustration(): ?bool
    {
        return $this->useAlternativeIllustration;
    }

    /**
     * @param ?bool $useAlternativeIllustration
     */
    public function setUseAlternativeIllustration(?bool $useAlternativeIllustration): void
    {
        $this->useAlternativeIllustration = $useAlternativeIllustration;
    }
}
