<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExerciseProgression.
 *
 * @ORM\Entity(repositoryClass="App\Repository\ExerciseProgressionRepository")
 */
class ExerciseProgression
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Exercise
     *
     * @ORM\ManyToOne(targetEntity="Exercise")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(referencedColumnName="id")
     * })
     */
    private $source;

    /**
     * @var Exercise
     *
     * @ORM\ManyToOne(targetEntity="Exercise")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(referencedColumnName="id")
     * })
     */
    private $target;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Exercise
     */
    public function getSource(): Exercise
    {
        return $this->source;
    }

    /**
     * @param Exercise $source
     */
    public function setSource(Exercise $source): void
    {
        $this->source = $source;
    }

    /**
     * @return Exercise
     */
    public function getTarget(): Exercise
    {
        return $this->target;
    }

    /**
     * @param Exercise $target
     */
    public function setTarget(Exercise $target): void
    {
        $this->target = $target;
    }

}
