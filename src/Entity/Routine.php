<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * Routine.
 *
 * @ORM\Entity(repositoryClass="App\Repository\RoutineRepository")
 */
class Routine implements TranslatableInterface
{
    use TranslatableTrait;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $name_id;

    /**
     * @var Level
     *
     * @ORM\ManyToOne(targetEntity="Level")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(referencedColumnName="id")
     * })
     */
    private $level;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $sortOrder;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $featuredOrder;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var RoutineSequence[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="RoutineSequence",
     *      mappedBy="routine",
     *      orphanRemoval=true,
     *      cascade={"persist"}
     * )
     * @ORM\OrderBy({"sequenceOrder": "ASC"})
     */
    private $routineSequences;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", length=100, nullable=true)
     */
    private $author;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", length=100, nullable=true)
     */
    private $link;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $minDuration;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $duration;

    /**
     * @var RoutineCategory
     *
     * @ORM\ManyToOne(targetEntity="RoutineCategory", inversedBy="routines")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(referencedColumnName="id")
     * })
     */
    private $routineCategory;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="routines")
     * @ORM\JoinTable(
     *   joinColumns={
     *     @ORM\JoinColumn(referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(referencedColumnName="id")
     *   }
     * )
     */
    private $tags;

    public function __construct()
    {
        $this->routineSequences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLevel(): ?Level
    {
        return $this->level;
    }

    public function setLevel(?Level $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRoutineSequences(): Collection
    {
        return $this->routineSequences;
    }

    /**
     * @return string|null
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string|null $link
     */
    public function setLink(?string $link): void
    {
        $this->link = $link;
    }

    /**
     * @return string|null
     */
    public function getAuthor(): ?string
    {
        return $this->author;
    }

    /**
     * @param string|null $author
     */
    public function setAuthor(?string $author): void
    {
        $this->author = $author;
    }

    /**
     * @return Collection
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return int
     */
    public function getFeaturedOrder(): ?int
    {
        return $this->featuredOrder;
    }

    /**
     * @param int $featuredOrder
     */
    public function setFeaturedOrder(int $featuredOrder): void
    {
        $this->featuredOrder = $featuredOrder;
    }

    /**
     * @return RoutineCategory
     */
    public function getRoutineCategory(): RoutineCategory
    {
        return $this->routineCategory;
    }

    /**
     * @param RoutineCategory $routineCategory
     */
    public function setRoutineCategory(RoutineCategory $routineCategory): void
    {
        $this->routineCategory = $routineCategory;
    }

    /**
     * @return string
     */
    public function getNameId(): string
    {
        return $this->name_id;
    }

    /**
     * @param string $name_id
     */
    public function setNameId(string $name_id): void
    {
        $this->name_id = $name_id;
    }

    /**
     * @return int
     */
    public function getMinDuration(): ?int
    {
        return $this->minDuration;
    }

    /**
     * @param int $minDuration
     */
    public function setMinDuration(?int $minDuration): void
    {
        $this->minDuration = $minDuration;
    }

    /**
     * @return int|null
     */
    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    /**
     * @param int|null $sortOrder
     * @return Routine
     */
    public function setSortOrder(?int $sortOrder): Routine
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }
}
