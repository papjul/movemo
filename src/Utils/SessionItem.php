<?php

namespace App\Utils;

class SessionItem
{
    private $id = null;
    private $nameId = null;
    private $name;
    private $quantity;
    private $minQuantity = null;
    private $isDuration = true;
    private $variant;
    private $currentSet = 1;
    private $maxSet = 1;
    private $useVideo = false;
    private $useAlternativeIllustration = false;
    private $note;
    private $sequenceId = null;
    private $type = null;

    public function getQuantityFormatted()
    {
        if ($this->isDuration) {
            if ($this->quantity < 60) {
                if ($this->minQuantity > 0) {
                    return $this->minQuantity.'-'.$this->quantity.' sec';
                } else {
                    return $this->quantity.' sec';
                }
            } elseif ($this->minQuantity > 0 && $this->minQuantity != $this->quantity) {
                $minutesQuantity = floor($this->quantity / 60);
                $secondsQuantity = $this->quantity % 60;

                $return = '';
                if ($this->minQuantity < 60) {
                    $return = $this->minQuantity.' sec-';
                } else {
                    $minutesMinQuantity = floor($this->minQuantity / 60);
                    $secondsMinQuantity = $this->minQuantity % 60;
                    if (0 != $secondsMinQuantity || 0 != $secondsQuantity) {
                        $return = $minutesQuantity.' min '.$secondsQuantity.'-';
                    } else {
                        return $minutesMinQuantity.'-'.$minutesQuantity.' min';
                    }
                }

                if (0 != $secondsQuantity) {
                    $return .= $minutesQuantity.' min '.$secondsQuantity;
                } else {
                    $return .= $minutesQuantity.' min';
                }

                return $return;
            } else {
                $minutes = floor($this->quantity / 60);
                $seconds = $this->quantity % 60;

                if (0 != $seconds) {
                    return $minutes.' min '.$seconds;
                } else {
                    return $minutes.' min';
                }
            }
        } else {
            if (0 == $this->quantity) {
                return 'MAX';
            } elseif ($this->minQuantity > 0) {
                return $this->minQuantity.'-'.$this->quantity.' reps';
            } else {
                return $this->quantity.' reps';
            }
        }
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name_id' => $this->getNameId(),
            'name' => $this->getName(),
            'quantity' => $this->getQuantity(),
            'min_quantity' => $this->getMinQuantity(),
            'is_duration' => $this->isDuration(),
            'variant' => $this->getVariant(),
            'current_set' => $this->getCurrentSet(),
            'max_set' => $this->getMaxSet(),
            'use_video' => $this->isUseVideo(),
            'use_alternative_illustration' => $this->isUseAlternativeIllustration(),
            'note' => $this->getNote(),
            'sequence_id' => $this->getSequenceId(),
            'type' => $this->getType(),
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getNameId()
    {
        return $this->nameId;
    }

    /**
     * @param null $nameId
     */
    public function setNameId($nameId): void
    {
        $this->nameId = $nameId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return null
     */
    public function getMinQuantity()
    {
        return $this->minQuantity;
    }

    /**
     * @param null $minQuantity
     */
    public function setMinQuantity($minQuantity): void
    {
        $this->minQuantity = $minQuantity;
    }

    public function isDuration(): bool
    {
        return $this->isDuration;
    }

    public function setIsDuration(bool $isDuration): void
    {
        $this->isDuration = $isDuration;
    }

    /**
     * @return mixed
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * @param mixed $variant
     */
    public function setVariant($variant): void
    {
        $this->variant = $variant;
    }

    public function getCurrentSet(): int
    {
        return $this->currentSet;
    }

    public function setCurrentSet(int $currentSet): void
    {
        $this->currentSet = $currentSet;
    }

    public function getMaxSet(): int
    {
        return $this->maxSet;
    }

    public function setMaxSet(int $maxSet): void
    {
        $this->maxSet = $maxSet;
    }

    public function isUseVideo(): bool
    {
        return $this->useVideo;
    }

    public function setUseVideo(bool $useVideo): void
    {
        $this->useVideo = $useVideo;
    }

    /**
     * @return bool
     */
    public function isUseAlternativeIllustration(): ?bool
    {
        return $this->useAlternativeIllustration;
    }

    /**
     * @param bool $useAlternativeIllustration
     */
    public function setUseAlternativeIllustration(?bool $useAlternativeIllustration): void
    {
        $this->useAlternativeIllustration = $useAlternativeIllustration;
    }

    /**
     * @return ?string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param ?string $note
     */
    public function setNote(?string $note): void
    {
        $this->note = $note;
    }

    /**
     * @return null
     */
    public function getSequenceId()
    {
        return $this->sequenceId;
    }

    /**
     * @param null $sequenceId
     */
    public function setSequenceId($sequenceId): void
    {
        $this->sequenceId = $sequenceId;
    }

    /**
     * @return null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param null $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }
}
