<?php

namespace App\Repository;

use App\Entity\Exercise;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;

class ExerciseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Exercise::class);
    }

    public function findExercises($locale)
    {
        // DO NOT remove English when coming from another locale. English translation is used on client side

        // This query is built in a way lazy-loading is avoided and all joins are properly sorted
        return $this->createQueryBuilder('e')
            ->addSelect('et')
            ->leftJoin('e.translations', 'et')
            //->where('et.locale = :locale') // Needed to not sort by ALL locales, but only the current locale
            ->orderBy('et.name', 'asc') // TODO: Find another way to sort
            //->setParameter('locale', $locale)
            ->getQuery()
            ->getArrayResult();
    }

    public function findExercise(string $name_id, $locale)
    {
        // This query is built in a way lazy-loading is avoided and all joins are properly sorted
        return $this->createQueryBuilder('e')
            ->addSelect('et')
            ->leftJoin('e.translations', 'et')
            ->where('e.name_id = :name_id')
            //->andWhere('et.locale = :locale') // Needed to not sort by ALL locales, but only the current locale
            //->setParameter('locale', $locale)
            ->setParameter('name_id', $name_id)
            ->getQuery()
            ->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function updateTranslation($id, $name_en, $name_fr)
    {
        $exercise = $this->find($id);
        $exercise->translate('en')->setName($name_en);
        $exercise->translate('fr')->setName($name_fr);
        $this->getDoctrine()->getManager()->persist($exercise);

        // In order to persist new translations, call mergeNewTranslations method, before flush
        $exercise->mergeNewTranslations();
        $this->getDoctrine()->getManager()->flush();
    }
}
