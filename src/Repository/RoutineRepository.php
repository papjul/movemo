<?php

namespace App\Repository;

use App\Entity\Routine;
use App\Utils\SessionItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;
use Symfony\Contracts\Translation\TranslatorInterface;

class RoutineRepository extends ServiceEntityRepository
{
    const REP_DURATION = 3;
    /** @var TranslatorInterface */
    private $translator;

    public function __construct(ManagerRegistry $registry, TranslatorInterface $translator)
    {
        parent::__construct($registry, Routine::class);
        $this->translator = $translator;
    }

    public function findFeaturedRoutines($locale)
    {
        // This query is built in a way lazy-loading is avoided and all joins are properly sorted
        return $this->createQueryBuilder('r')
            ->orderBy('r.featuredOrder', 'ASC')
            ->addSelect('rc')
            ->leftJoin('r.routineCategory', 'rc')
            ->addSelect('rt')
            ->leftJoin('r.translations', 'rt')
            ->where('r.featuredOrder IS NOT NULL')
            //->andWhere('rt.locale = :locale') // Needed to not sort by ALL locales, but only the current locale
            ->addOrderBy('rt.name', 'asc')
            //->setParameter('locale', $locale)
            ->addSelect('l', 'lt', 't', 'tt', 'rct')
            ->leftJoin('r.level', 'l')
            ->leftJoin('l.translations', 'lt')
            ->leftJoin('r.tags', 't')
            ->leftJoin('t.translations', 'tt')
            ->leftJoin('rc.translations', 'rct')
            ->getQuery()
            ->getArrayResult();
    }

    public function findRoutine(string $name_id, $locale)
    {
        // This query is built in a way lazy-loading is avoided and all joins are properly sorted
        $routine = $this->createQueryBuilder('r')
            ->addSelect('rt')
            ->leftJoin('r.translations', 'rt')
            ->where('r.name_id = :name_id')
            //->andWhere('rt.locale = :locale') // Needed to not sort by ALL locales, but only the current locale
            //->setParameter('locale', $locale)
            ->setParameter('name_id', $name_id)
            ->getQuery()
            ->getSingleResult(AbstractQuery::HYDRATE_ARRAY);

        return $routine;
    }

    // TODO: This can be simplified by setting exercise as entity and using a fallback strategy for translations on client side
    // TODO: Also, exercises need to ALWAYS be an array even if only one exercise, for normalizr
    public function getSessionItems(Routine $routine, $locale, $fallbackLocale = 'en'): array
    {
        $sessionItems = [];
        foreach ($routine->getRoutineSequences() as $routineSequence) {
            $nbSets = $routineSequence->getNbSets();
            if ($nbSets < 1) {
                $nbSets = 1;
            }
            $sequenceSessionItems = [];
            for ($i = 0; $i < $nbSets; ++$i) {
                foreach ($routineSequence->getSequenceItems() as $sequenceItem) {
                    $sessionItem = new SessionItem();
                    if (null != $sequenceItem->getExercise()) {
                        $sessionItem->setId($sequenceItem->getExercise()->getId());
                        $sessionItem->setNameId($sequenceItem->getExercise()->getNameId());
                        $sessionItem->setIsDuration($sequenceItem->isDuration());
                        if (null != $sequenceItem->getExercise()->translate($locale)->getName() && '' != $sequenceItem->getExercise()->translate($locale)->getName()) {
                            $sessionItem->setName($sequenceItem->getExercise()->translate($locale)->getName());
                        } elseif (null != $sequenceItem->getExercise()->translate($fallbackLocale)->getName() && '' != $sequenceItem->getExercise()->translate($fallbackLocale)->getName()) {
                            $sessionItem->setName($sequenceItem->getExercise()->translate($fallbackLocale)->getName());
                        }
                        $sessionItem->setQuantity($sequenceItem->getQuantity());
                        $sessionItem->setMinQuantity($sequenceItem->getMinQuantity());
                        if (null != $sequenceItem->translate($locale)->getVariant() && '' != $sequenceItem->translate($locale)->getVariant()) {
                            $sessionItem->setVariant($sequenceItem->translate($locale)->getVariant());
                        } elseif (null != $sequenceItem->translate($fallbackLocale)->getVariant() && '' != $sequenceItem->translate($fallbackLocale)->getVariant()) {
                            $sessionItem->setVariant($sequenceItem->translate($fallbackLocale)->getVariant());
                        }
                        if (null != $sequenceItem->translate($locale)->getNote() && '' != $sequenceItem->translate($locale)->getNote()) {
                            $sessionItem->setNote($sequenceItem->translate($locale)->getNote());
                        } elseif (null != $sequenceItem->translate($fallbackLocale)->getNote() && '' != $sequenceItem->translate($fallbackLocale)->getNote()) {
                            $sessionItem->setNote($sequenceItem->translate($fallbackLocale)->getNote());
                        }
                        $sessionItem->setCurrentSet($i + 1);
                        $sessionItem->setMaxSet($nbSets);
                        $sessionItem->setUseVideo($sequenceItem->getExercise()->getHasVideo() && !$sequenceItem->isUseStaticImage());
                        $sessionItem->setUseAlternativeIllustration($sequenceItem->isUseAlternativeIllustration());
                    }
                    // Fixme name of method
                    $sessionItem->setSequenceId($routineSequence->getId());
                    $sessionItem->setType($routineSequence->getType());
                    $sequenceSessionItems[] = $sessionItem;
                }
                if ($i < $nbSets - 1 && $routineSequence->getSecBetweenSets() > 0) {
                    $sessionItem = new SessionItem();
                    $sessionItem->setName($this->translator->trans('name.rest'));
                    $sessionItem->setQuantity($routineSequence->getSecBetweenSets());
                    $sessionItem->setCurrentSet($i + 1);
                    $sessionItem->setMaxSet($nbSets);
                    $sequenceSessionItems[] = $sessionItem;
                }
            }
            if ($routineSequence->getSecAfterSequence() > 0) {
                $sessionItem = new SessionItem();
                $sessionItem->setName($this->translator->trans('name.rest'));
                $sessionItem->setQuantity($routineSequence->getSecAfterSequence());
                $sessionItem->setCurrentSet($nbSets);
                $sessionItem->setMaxSet($nbSets);
                $sequenceSessionItems[] = $sessionItem;
            }
            if ('progression' === $routineSequence->getType()) {
                $sessionItems[] = $sequenceSessionItems;
            } else {
                foreach ($sequenceSessionItems as $sequenceSessionItem) {
                    $sessionItems[] = $sequenceSessionItem;
                }
            }
        }

        return $sessionItems;
    }

    private function getDuration(array $sessionItems): int
    {
        $duration = 0;
        foreach ($sessionItems as $sessionItem) {
            if (is_array($sessionItem)) {
                if ($sessionItem[0]->isDuration()) {
                    $duration += $sessionItem[0]->getQuantity();
                } else {
                    $duration += $sessionItem[0]->getQuantity() * self::REP_DURATION;
                }
            } else {
                if ($sessionItem->isDuration()) {
                    $duration += $sessionItem->getQuantity();
                } else {
                    $duration += $sessionItem->getQuantity() * self::REP_DURATION;
                }
            }
        }

        return $duration;
    }
}
