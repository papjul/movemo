<?php

namespace App\Repository;

use App\Entity\ExerciseProgression;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr;

class ExerciseProgressionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExerciseProgression::class);
    }

    public function findProgressions($locale)
    {
        // This query is built in a way lazy-loading is avoided and all joins are properly sorted
        return $this->createQueryBuilder('p')
            ->addSelect('ps')
            ->leftJoin('p.source', 'ps')
            ->addSelect('pt')
            ->leftJoin('p.target', 'pt')
            ->getQuery()
            ->getArrayResult();
    }

    public function getProgressionExercises($locale)
    {
        $query = $this->createQueryBuilder('ep')
            ->select('ep, es, et, esl, etl')
            ->join('ep.source', 'es')
            ->join('ep.target', 'et')
            ->join('es.level', 'esl')
            ->join('et.level', 'etl')
            ->addSelect('est')
            ->leftJoin('es.translations', 'est', Expr\Join::WITH, 'est.locale = :locale')
            ->addSelect('ett')
            ->leftJoin('et.translations', 'ett', Expr\Join::WITH, 'ett.locale = :locale')
            ->setParameter('locale', $locale)
            ->getQuery();
        $arrayResults = $query->getResult(Query::HYDRATE_ARRAY);

        $returnResults = [];
        foreach ($arrayResults as $exerciseProgression) {
            if (!array_key_exists($exerciseProgression['source']['id'], $returnResults)) {
                $returnResults[$exerciseProgression['source']['id']] = [
                    'name_id' => $exerciseProgression['source']['name_id'],
                    'name' => (isset($exerciseProgression['source']['translations'][$locale])) ? $exerciseProgression['source']['translations'][$locale]['name'] : $exerciseProgression['source']['name_id'],
                    'level' => $exerciseProgression['source']['level']['name_id'],
                    'milestone' => $exerciseProgression['source']['milestone'],
                ];
            }
            if (!array_key_exists($exerciseProgression['target']['id'], $returnResults)) {
                $returnResults[$exerciseProgression['target']['id']] = [
                    'name_id' => $exerciseProgression['target']['name_id'],
                    'name' => (isset($exerciseProgression['target']['translations'][$locale])) ? $exerciseProgression['target']['translations'][$locale]['name'] : $exerciseProgression['target']['name_id'],
                    'level' => $exerciseProgression['target']['level']['name_id'],
                    'milestone' => $exerciseProgression['target']['milestone'],
                ];
            }
        }

        return $returnResults;
    }
}
