<?php

namespace App\Repository;

use App\Entity\RoutineCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class RoutineCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoutineCategory::class);
    }

    public function findRoutineCategoryWithRoutines($locale)
    {
        // This query is built in a way lazy-loading is avoided and all joins are properly sorted
        return $this->createQueryBuilder('rc')
            ->orderBy('rc.sortOrder', 'asc')
            ->addSelect('r, CASE WHEN r.sortOrder IS NULL THEN 1 ELSE 0 END AS HIDDEN sortOrderNullLast') // Trick to get nulls sorted at the end
            ->leftJoin('rc.routines', 'r')
            ->addOrderBy('sortOrderNullLast', 'asc')
            ->addOrderBy('r.sortOrder', 'asc')
            ->addSelect('rt')
            ->leftJoin('r.translations', 'rt')
            //->where('rt.locale = :locale') // Needed to not sort by ALL locales, but only the current locale
            ->addOrderBy('rt.name', 'asc') // TODO: Find another way to sort
            //->setParameter('locale', $locale)
            ->addSelect('l', 'lt', 't', 'tt', 'rct')
            ->leftJoin('r.level', 'l')
            ->leftJoin('l.translations', 'lt')
            ->leftJoin('r.tags', 't')
            ->leftJoin('t.translations', 'tt')
            ->leftJoin('rc.translations', 'rct')
            ->getQuery()
            ->getArrayResult();
    }
}
