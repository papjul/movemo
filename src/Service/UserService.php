<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\ExpiredTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\InvalidTokenException;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\PreAuthenticationJWTUserToken;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\CookieTokenExtractor;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;

class UserService
{
    const ALLOWED_LOCALES = ['en', 'fr'];
    const ALLOWED_THEMES = ['light', 'dark'];
    const ALLOWED_FONTS = ['default', 'luciole', 'opendys'];

    /**
     * @var JWTTokenManagerInterface
     */
    private $JWTManager;

    /**
     * @var JWTEncoderInterface
     */
    private $JWTEncoder;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(JWTTokenManagerInterface $JWTManager, JWTEncoderInterface $JWTEncoder, EntityManagerInterface $em)
    {
        $this->JWTManager = $JWTManager;
        $this->JWTEncoder = $JWTEncoder;
        $this->em = $em;
    }

    public function getInitialState(Request $request)
    {
        return [
            'authToken' => $this->getValidToken($request),
            'locale' => $this->getLocale($request),
            'theme' => $this->getTheme($request),
            'font' => $this->getFont($request),
        ];
    }

    public function getCurrentUser(Request $request): ?User
    {
        $token = $this->getValidToken($request);
        if (null === $token) {
            return null;
        }

        try {
            $data = $this->JWTEncoder->decode($token);
        } catch (JWTDecodeFailureException $e) {
            throw new CustomUserMessageAuthenticationException('Invalid Token');
        }

        return $this->em->getRepository(User::class)->findOneBy(['username' => $data['username']]);
    }

    public function getLocale(Request $request): string
    {
        return $request->getLocale();
    }

    public function getTheme(Request $request): string
    {
        $user = $this->getCurrentUser($request);
        if (null !== $user && in_array($user->getTheme(), UserService::ALLOWED_THEMES)) {
            return $user->getTheme();
        }

        return 'light';
    }

    public function getFont(Request $request): string
    {
        /*$user = $this->getCurrentUser($request);
        if (null !== $user && in_array($user->getFont(), UserService::ALLOWED_FONTS)) {
            return $user->getFont();
        }*/

        return 'default';
    }

    public function getValidToken(Request $request)
    {
        $tokenExtractor = new CookieTokenExtractor('BEARER');

        if (false === ($jsonWebToken = $tokenExtractor->extract($request))) {
            return null;
        }

        $preAuthToken = new PreAuthenticationJWTUserToken($jsonWebToken);
        try {
            if (!$payload = $this->JWTManager->decode($preAuthToken)) {
                throw new InvalidTokenException('Invalid JWT Token');
            }
            $preAuthToken->setPayload($payload);
        } catch (JWTDecodeFailureException $e) {
            if (JWTDecodeFailureException::EXPIRED_TOKEN === $e->getReason()) {
                throw new ExpiredTokenException();
            }
            throw new InvalidTokenException('Invalid JWT Token', 0, $e);
        }

        return $preAuthToken->getCredentials();
    }
}
