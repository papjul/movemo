<?php

namespace App\Controller;

use App\Entity\ExerciseProgression;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Controller used to manage blog contents in the public part of the site.
 */
class ProgressionController extends AbstractController
{
    /**
     * @Route("/skills", methods="GET", name="skills")
     *
     * @return Response
     */
    public function skills(Request $request, SerializerInterface $serializer, UserService $userService)
    {
        if (null === $userService->getCurrentUser($request)) {
            return $this->redirectToRoute('login');
        }

        $initialState = $userService->getInitialState($request);
        $initialState['progressionExercises'] = $this->getDoctrine()->getRepository(ExerciseProgression::class)->getProgressionExercises($request->getLocale());
        $initialState['skills'] = $this->getDoctrine()->getRepository(ExerciseProgression::class)->findProgressions($request->getLocale());

        return $this->render('index.html.twig', [
            'initialState' => $serializer->normalize($initialState),
        ]);
    }
}
