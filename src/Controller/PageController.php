<?php

namespace App\Controller;

use App\Entity\Page;
use App\Entity\Routine;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class PageController extends AbstractController
{
    /**
     * @Route("", methods="GET", name="homepage")
     *
     * @return Response
     */
    public function homepage(Request $request, SerializerInterface $serializer, UserService $userService)
    {
        if (null === $userService->getCurrentUser($request)) {
            return $this->redirectToRoute('login');
        }

        $initialState = $userService->getInitialState($request);
        $initialState['featuredRoutines'] = $this->getDoctrine()->getRepository(Routine::class)->findFeaturedRoutines($request->getLocale());

        return $this->render('index.html.twig', [
            'initialState' => $serializer->normalize($initialState),
        ]);
    }

    /**
     * @Route("/login", methods="GET", name="login")
     *
     * @return Response
     */
    public function login(Request $request, SerializerInterface $serializer, UserService $userService)
    {
        $initialState = $userService->getInitialState($request);

        return $this->render('index.html.twig', [
            'initialState' => $serializer->normalize($initialState),
        ]);
    }

    /**
     * @Route("/relaxation", methods="GET", name="relaxation")
     *
     * @return Response
     */
    public function relaxation(Request $request, SerializerInterface $serializer, UserService $userService)
    {
        $initialState = $userService->getInitialState($request);

        return $this->render('index.html.twig', [
            'initialState' => $serializer->normalize($initialState),
        ]);
    }

    /**
     * @Route("/about", methods="GET", name="about")
     */
    public function about(Request $request, UserService $userService)
    {
        if (null === $userService->getCurrentUser($request)) {
            return $this->redirectToRoute('login');
        }

        return $this->render('page/about.html.twig');
    }
}
