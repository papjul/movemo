<?php

namespace App\Controller;

use App\Entity\Exercise;
use App\Entity\Routine;
use App\Repository\ExerciseRepository;
use App\Repository\MuscleRepository;
use App\Service\UserService;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Exercise Controller.
 */
class ExerciseController extends AbstractController
{
    /**
     * @Route("/exercises", methods="GET", name="exercise_index")
     */
    public function index(Request $request, SerializerInterface $serializer, UserService $userService): Response
    {
        if (null === $userService->getCurrentUser($request)) {
            return $this->redirectToRoute('login');
        }

        $initialState = $userService->getInitialState($request);
        $initialState['exercises'] = $this->getDoctrine()->getRepository(Exercise::class)->findExercises($request->getLocale());

        return $this->render('index.html.twig', [
            'initialState' => $serializer->normalize($initialState),
        ]);
    }

    /**
     * @Route("/exercises/{name_id}", methods="GET", name="exercise_view")
     *
     * NOTE: The $post controller argument is automatically injected by Symfony
     * after performing a database query looking for a Post with the 'slug'
     * value given in the route.
     * See https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/converters.html
     */
    public function exerciseView(Request $request, SerializerInterface $serializer, UserService $userService, JWTTokenManagerInterface $JWTManager, string $name_id): Response
    {
        if (null === $userService->getCurrentUser($request)) {
            return $this->redirectToRoute('login');
        }

        $initialState = $userService->getInitialState($request);
        $initialState['exercise'] = $this->getDoctrine()->getRepository(Exercise::class)->findExercise($name_id, $request->getLocale());

        return $this->render('index.html.twig', [
            'initialState' => $serializer->normalize($initialState),
        ]);
    }
}
