<?php

namespace App\Controller;

use App\Form\Type\ChangePasswordType;
use App\Service\UserService;
use Limenius\Liform\Liform;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Controller used to manage current user.
 */
class UserController extends AbstractController
{
    /**
     * @Route("/profile/password", methods="GET|POST", name="user_password")
     */
    public function changePassword(Request $request, UserService $userService, UserPasswordEncoderInterface $encoder): Response
    {
        if (null === $userService->getCurrentUser($request)) {
            return $this->redirectToRoute('login');
        }

        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($encoder->encodePassword($user, $form->get('newPassword')->getData()));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('app_logout');
        }

        return $this->render('user/change_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/account", methods="GET", name="account")
     *
     * @return Response
     */
    public function account(Request $request, SerializerInterface $serializer, UserService $userService, Liform $liform)
    {
        if (null === $userService->getCurrentUser($request)) {
            return $this->redirectToRoute('login');
        }

        $initialState = $userService->getInitialState($request);
        $form = $this->createForm(ChangePasswordType::class);
        $initialState['changePasswordForm'] = $liform->transform($form);

        return $this->render('index.html.twig', [
            'initialState' => $serializer->normalize($initialState),
        ]);
    }
}
