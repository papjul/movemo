<?php

namespace App\Controller;

use App\Entity\Routine;
use App\Entity\RoutineCategory;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Controller used to manage routine contents in the public part of the site.
 */
class RoutineController extends AbstractController
{
    /**
     * @Route("/routines", methods="GET", name="routine_index")
     *
     * @throws ExceptionInterface
     */
    public function index(Request $request, UserService $userService, SerializerInterface $serializer): Response
    {
        if (null === $userService->getCurrentUser($request)) {
            return $this->redirectToRoute('login');
        }

        $initialState = $userService->getInitialState($request);
        $initialState['routineCategories'] = $this->getDoctrine()->getRepository(RoutineCategory::class)->findRoutineCategoryWithRoutines($request->getLocale());

        return $this->render('index.html.twig', [
            'initialState' => $serializer->normalize($initialState),
        ]);
    }

    /**
     * @Route("/routines/{name_id}", methods="GET", name="routine_view")
     * @Route("/routines/{name_id}/run", methods="GET", name="routine_run")
     *
     * NOTE: The $post controller argument is automatically injected by Symfony
     * after performing a database query looking for a Post with the 'slug'
     * value given in the route.
     * See https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/converters.html
     */
    public function routineViewAndRun(Request $request, Routine $routine, string $name_id, SerializerInterface $serializer, UserService $userService): Response
    {
        if (null === $userService->getCurrentUser($request)) {
            return $this->redirectToRoute('login');
        }

        $initialState = $userService->getInitialState($request);
        $initialState['routine'] = $this->getDoctrine()->getRepository(Routine::class)->findRoutine($name_id, $request->getLocale());
        $initialState['routine']['items'] = $this->getDoctrine()->getRepository(Routine::class)->getSessionItems($routine, $request->getLocale()); // Redundant, fetch two times Routine

        return $this->render('index.html.twig', [
            'initialState' => $serializer->normalize($initialState),
        ]);
    }
}
