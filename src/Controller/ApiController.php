<?php

namespace App\Controller;

use App\Entity\Exercise;
use App\Entity\ExerciseProgression;
use App\Entity\Routine;
use App\Entity\RoutineCategory;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends AbstractController
{
    /**
     * @Rest\Get("/api/featured-routines.json")
     * @Rest\Get("/api/featured-routines")
     * @Rest\View()
     */
    public function featuredRoutinesApi(Request $request)
    {
        return $this->getDoctrine()->getRepository(Routine::class)->findFeaturedRoutines($request->getLocale());
    }

    /**
     * @Rest\Get("/api/routine-categories.json")
     * @Rest\Get("/api/routine-categories")
     * @Rest\View()
     */
    public function getRoutinesApi(Request $request)
    {
        return $this->getDoctrine()->getRepository(RoutineCategory::class)->findRoutineCategoryWithRoutines($request->getLocale());
    }

    /**
     * @Rest\Get("/api/routines/{name_id}.json")
     * @Rest\Get("/api/routines/{name_id}")
     * @Rest\View()
     */
    public function getRoutineApi(Request $request, Routine $routine, string $name_id)
    {
        $routineArray = $this->getDoctrine()->getRepository(Routine::class)->findRoutine($name_id, $request->getLocale());
        $routineArray['items'] = $this->getDoctrine()->getRepository(Routine::class)->getSessionItems($routine, $request->getLocale()); // Redundant, fetch two times Routine
        return $routineArray;
    }

    /**
     * @Rest\Get("/api/skills.json")
     * @Rest\Get("/api/skills")
     * @Rest\View()
     */
    public function getSkillsApi(Request $request)
    {
        return $this->getDoctrine()->getRepository(ExerciseProgression::class)->findProgressions($request->getLocale());
    }

    /**
     * @Rest\Get("/api/progression-exercises.json")
     * @Rest\Get("/api/progression-exercises")
     * @Rest\View()
     */
    public function getProgressionExercisesApi(Request $request)
    {
        return $this->getDoctrine()->getRepository(ExerciseProgression::class)->getProgressionExercises($request->getLocale());
    }

    /**
     * @Rest\Get("/api/exercises.json")
     * @Rest\Get("/api/exercises")
     * @Rest\View()
     */
    public function getExercisesApi(Request $request)
    {
        return $this->getDoctrine()->getRepository(Exercise::class)->findExercises($request->getLocale());
    }

    /**
     * @Rest\Get("/api/exercises/{name_id}.json")
     * @Rest\Get("/api/exercises/{name_id}")
     * @Rest\View()
     */
    public function getExerciseApi(Request $request, string $name_id)
    {
        return $this->getDoctrine()->getRepository(Exercise::class)->findExercise($name_id, $request->getLocale());
    }
}
