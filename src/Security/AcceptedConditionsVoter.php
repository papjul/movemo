<?php

namespace App\Security;

use App\Entity\User;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AcceptedConditionsVoter extends Voter
{
    const PRIVACY = 'privacy';
    const TERMS = 'terms';

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::PRIVACY, self::TERMS])) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // Add some logic to accept terms in case not logged
            return false;
        }

        switch ($attribute) {
            case self::PRIVACY:
                return $this->hasAcceptedPrivacyPolicy($user);
            case self::TERMS:
                return $this->hasAcceptedTerms($user);
        }

        throw new LogicException('This code should not be reached!');
    }

    private function hasAcceptedPrivacyPolicy(User $user): bool
    {
        return true;
    }

    private function hasAcceptedTerms(User $user): bool
    {
        return true;
    }
}
