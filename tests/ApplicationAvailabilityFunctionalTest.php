<?php

declare(strict_types=1);

namespace App\Tests;

use Generator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ApplicationAvailabilityFunctionalTest.
 */
class ApplicationAvailabilityFunctionalTest extends WebTestCase
{
    /**
     * @dataProvider urlProvider
     */
    public function testEnglishPageIsSuccessful(string $url)
    {
        $client = self::createClient();
        $client->request('GET', '/en'.$url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    /**
     * @dataProvider urlProvider
     */
    public function testFrenchPageIsSuccessful(string $url)
    {
        $client = self::createClient();
        $client->request('GET', '/fr'.$url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    /**
     * @dataProvider urlRedirectProvider
     */
    public function testEnglishPageRedirects(string $url)
    {
        $client = self::createClient();
        $client->request('GET', '/en'.$url);

        $this->assertTrue($client->getResponse()->isRedirection());
    }

    /**
     * @dataProvider urlRedirectProvider
     */
    public function testFrenchPageRedirects(string $url)
    {
        $client = self::createClient();
        $client->request('GET', '/fr'.$url);

        $this->assertTrue($client->getResponse()->isRedirection());
    }

    /**
     * @return Generator
     */
    public function urlRedirectProvider()
    {
        yield [''];

        // Routines
        yield ['/routines'];
        yield ['/routines/joint'];
        yield ['/routines/joint/run'];
        yield ['/routines/recommended_routine'];
        yield ['/routines/recommended_routine/run'];

        // Exercises
        yield ['/exercises'];
        yield ['/exercises/prone_extension'];

        // Skills
        yield ['/progression'];
        yield ['/skills'];

        // Pages
        yield ['/about'];

        // Other
        yield ['/profile/password'];
    }

    /**
     * @return Generator
     */
    public function urlProvider()
    {
        // Relaxation
        yield ['/relaxation'];

        // User
        yield ['/login'];
    }
}
